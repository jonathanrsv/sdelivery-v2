<?php
$secao =  $this->uri->segment(2);
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
	<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
	<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
	<html class="no-js lt-ie9"> <![endif]-->
	<!--[if gt IE 8]><!-->
	<html class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo $title; ?> - <?php echo $project_name; ?></title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php echo link_tag('public/assets/css/vendor/normalize.css'); ?>
		<?php echo link_tag('public/assets/css/vendor/bootstrap.min.css'); ?>
		<?php echo link_tag('public/assets/css/vendor/avgrund.css'); ?>
		<?php echo link_tag('public/assets/css/vendor/alertify.css'); ?>
		<?php echo link_tag('public/assets/css/vendor/imgareaselect-default.css'); ?>
		<?php echo link_tag('public/assets/css/vendor/imgareaselect-animated.css'); ?>
		<?php echo link_tag('public/assets/css/vendor/jquery.awesome-cropper.css'); ?>
		<?php echo link_tag('public/assets/css/vendor/bootstrap-editable.css'); ?>
		<?php echo link_tag('public/assets/css/vendor/summernote.css'); ?>
		<?php echo link_tag('public/assets/css/main.css'); ?>

		<script src="<?php echo base_url('public/assets/js/vendor/modernizr-2.6.2.min.js'); ?>"></script>


		<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">



</head>
<body>
	<div class="navbar navbar-default navbar-inverse navbar-static-top navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="<?= panel_anchor()?>"><?=$project_name;?></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li <?php echo $secao == ('dashboard') ? 'class="active"' :  '' ;?>>
						<a href="<?=panel_anchor('dashboard')?>">
							<i class="fa fa-dashboard"></i> &nbsp;Dashboard</a>
						</li>
						<li <?php echo $secao == ('categories') ? 'class="active"' :  '' ;?>>
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="fa fa-desktop"></i> &nbsp;Loja Online <i class="fa fa-caret-down"></i>
							</a>
							<ul class="dropdown-menu" role="menu">
								<li>
									<a href="<?= panel_anchor('categories')?>"><i class="fa fa-caret-right"></i>&nbsp; Gerenciar Categorias</a>
								</li>

                                <li>
                                    <a href="<?= panel_anchor('products')?>"><i class="fa fa-caret-right"></i>&nbsp; Gerenciar Produtos</a>
                                </li>

							</ul>
						</li>

                        <li <?php echo $secao == ('customers') ? 'class="active"' :  '' ;?>>
                            <a href="<?= panel_anchor('customers')?>">
                                <i class="fa fa-group"></i>&nbsp; Clientes
                            </a>
                        </li>

                        <li <?php echo $secao == ('config') ? 'class="active"' :  '' ;?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-desktop"></i> &nbsp;Sistema <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="<?= panel_anchor('config')?>"><i class="fa fa-caret-right"></i>&nbsp; Configurações</a>
                                </li>
                                <li>
                                    <a href="<?= panel_anchor('payment')?>"><i class="fa fa-caret-right"></i>&nbsp; Formas de Pagamento</a>
                                </li>


                            </ul>
                        </li>

										</ul>
										<ul class="nav navbar-nav navbar-right">
											<li class="dropdown">
												<a href="#" class="dropdown-toggle" data-toggle="dropdown">
													Olá,
													Jonathan Rodrigues &nbsp; <i class="fa fa-caret-down"></i>
												</a>
												<ul class="dropdown-menu">
													<li><a href="/panel/users/edit/72">
														<i class="fa fa-edit"></i> Alterar Dados</a></li>
														<li class="divider"></li>
														<li><a href="<?=panel_anchor('login/logout')?>">
															<i class="fa fa-sign-out"></i> Sair
														</a></li>
													</ul>
												</li>
											</ul>
										</div>
										<!--/.nav-collapse -->
									</div>
								</div>
								<?php include 'alerts.php'; ?>
								<?php echo $contents; ?>



								<script src="<?php echo base_url('public/assets/js/plugins.js'); ?>"></script>
								<script src="<?php echo base_url('public/assets/js/vendor/jquery-1.10.2.min.js'); ?>"></script>
								<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular.min.js"></script>
								<script src="<?php echo base_url('public/assets/js/vendor/bootstrap.min.js'); ?>"></script>
								<script src="<?php echo base_url('public/assets/js/vendor/handlebars.js'); ?>"></script>
								<script src="<?php echo base_url('public/assets/js/vendor/jquery.avgrund.min.js'); ?>"></script>
								<script src="<?php echo base_url('public/assets/js/vendor/bootbox.min.js'); ?>"></script>
								<script src="<?php echo base_url('public/assets/js/vendor/holder.js'); ?>"></script>
								<script src="<?php echo base_url('public/assets/js/vendor/alertify.js'); ?>"></script>
								<script src="<?php echo base_url('public/assets/js/vendor/jquery.awesome-cropper.js'); ?>"></script>
								<script src="<?php echo base_url('public/assets/js/vendor/bootstrap-editable.js'); ?>"></script>
								<script src="<?php echo base_url('public/assets/js/vendor/angular.slugfy.js'); ?>"></script>
								<script src="<?php echo base_url('public/assets/js/vendor/mask.js'); ?>"></script>
								<script src="<?php echo base_url('public/assets/js/vendor/bootstrap-datepicker.js'); ?>"></script>
								<script src="<?php echo base_url('public/assets/js/vendor/summernote.min.js'); ?>"></script>
                                <script src="<?php echo base_url('public/assets/js/vendor/moment.min.js'); ?>"></script>

								<script src="<?php echo base_url('public/assets/js/main.js'); ?>"></script>

</body>
</html>
