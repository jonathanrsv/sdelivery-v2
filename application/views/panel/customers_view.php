<div class="navbar-second">
    <div class="container">
        <div class="row">
            <div class="text-left col-md-6">
                <ol class="breadcrumb"><li><a href="<?= panel_anchor(); ?>">Panel</a></li> <li>Dashboard</li> <li class="active"> Clientes </li> </ol>
            </div>
            <div class="text-right col-md-6">
                <a class="btn btn-primary simple-insert" data-page="algo>" data-id-group="<?php //echo $group->id?>" href="#"><i class="fa fa-plus"></i>&nbsp; Adicionar algo</a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-filter"></i> Filtro
                </div>
                <div class="panel-body">
                    <form action="" method="get">
                        <div class="form-group row">
                            <div class="col-md-12">
                                <label for="inputText" class="control-label">Nome</label>
                                <input type="text" name="nome" class="form-control" id="inputText" value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 text-right">
                                <button type="submit" class="btn btn-primary btn-sm">
                                    <i class="fa fa-filter"></i> Filtar
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-reorder"></i> Clientes Cadastrados
                </div>
                <div class="panel-body">
                    <?php //dump($clientes)?>
                    <table class="table table-striped table-responsive table-hover table-centered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Opções</th>
                            <th>Nome</th>
                            <th>CPF</th>
                            <th>E-mail</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($clientes as $key => $cliente): ?>
                            <tr>
                                <td><?=$cliente->id_cliente?></td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-warning btn-group-sm btn-sm dropdown-toggle" data-toggle="dropdown">
                                            <i class="fa fa-cogs"></i> &nbsp; <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="<?=panel_anchor("$page_ref/edit/$cliente->id_cliente")?>"><i class="fa fa-edit"></i> Editar</a></li>
                                            <li><a href="<?=panel_anchor("$page_ref/remove/$cliente->id_cliente")?>" data-page="news" data-id="3" class="remove"><i class="fa fa-trash-o"></i> Remover</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td><?=$cliente->nome?></td>
                                <td><?=$cliente->cpf?></td>
                                <td><?=$cliente->email?></td>
                                <td><?=$cliente->status?></td>
                            </tr>
                        <?php endforeach ?>

                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
</div>