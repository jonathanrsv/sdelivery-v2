
<div class="navbar-second">
    <div class="container">
        <div class="row">
            <div class="text-left col-md-6">
                <ol class="breadcrumb"><li><a href="<?= panel_anchor(); ?>">Panel</a></li> <li>Dashboard</li> <li class="active"> Pedidos </li> </ol>
            </div>
            <div class="text-right col-md-6">
                <a class="btn btn-primary simple-insert" data-page="algo>" data-id-group="<?php //echo $group->id?>" href="#"><i class="fa fa-plus"></i>&nbsp; Adicionar algo</a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-reorder"></i> Itens do Pedido
                </div>
                <div class="panel-body">
                    <?php //dump($orders)?>
                    <table class="table table-striped table-responsive table-hover table-centered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Cliente</th>
                            <th>CPF</th>
                            <th>Realizado em</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php foreach ($orders as $key => $value): ?>

                            <tr <?php echo $value->status_pedido == 'Recebido' ? 'class="danger"' :  '' ;?> onclick="document.location = '<?=panel_anchor("$page_ref/pedido/$value->id_pedido")?>'" style="cursor:pointer;">
                                <td><?=$value->id_pedido?></td>
                                <td><?=$value->nome?></td>
                                <td><?=$value->cpf?></td>
                                <td class="date_order"><?=$value->data_pedido; ?></td>
                                <td><?=$value->status_pedido?></td>

                            </tr>
                        <?php endforeach ?>


                        </tbody>
                    </table>

                </div>


            </div>

        </div>
    </div>
</div>













