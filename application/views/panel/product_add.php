<div class="navbar-second">
    <div class="container">
        <div class="row">
            <div class="text-left col-md-6">
                <ol class="breadcrumb"><li><a href="<?= panel_anchor(); ?>">Panel</a></li><li>Produtos</li><li class="active">Editar</li></ol>
            </div>
            <div class="text-right col-md-6">
                <a class="btn btn-primary simple-insert" data-page="algo>" data-id-group="<?php //echo $group->id?>" href="#"><i class="fa fa-plus"></i>&nbsp; Adicionar algo</a>
            </div>
        </div>
    </div>
</div>
<div class="container">

    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-filter"></i> Imagem
                </div>
                <div class="panel-body">
                    
                    <form id="submitThis" method="post" action="<?=panel_anchor("$page_ref/update/")?>" class="form-horizontal"  accept-charset="utf-8" enctype="multipart/form-data" role="form" >

                        <span class="btn btn-danger btn-file">
                        Selecionar Imagem <input type="file" name="userfile">
                    </span>




                </div>
            </div>
        </div>

        <div class="col-md-9">

                <div class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-edit"></i>&nbsp; Adicionar Novo Produto
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> Nome do produto<span class="required">*</span></label>
                            <div class="col-md-6">
                                
                                <input type="text" name="nome" value="" class="form-control" id="inputUser" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> Categoria<span class="required">*</span></label>
                            <div class="col-md-6">
                                <select name="id_categoria" class="form-control">
                                    <option value="">Selecione uma categoria</option>
                                    <?php foreach ($categories as $key => $category): ?>
                                       
                                            <option value="<?=$category->id_categoria?>"><?=$category->nome?></option>
                                       
                                    <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> Qtd Disponivel<span class="required">*</span></label>
                            <div class="col-md-1">
                                <input type="text" name="qtd_estoque" value="" class="form-control" id="inputUser" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> Preço<span class="required">*</span></label>
                            <div class="col-md-2">
                                <input type="text" name="valor" value="" class="form-control" id="inputUser" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> Breve Descrição<span class="required">*</span></label>
                            <div class="col-md-6">
                                <textarea name="descricao" class="form-control" rows="3"></textarea>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="bs-callout bs-callout-warning">
                                <p> <i class="fa fa-warning"></i>&nbsp; <b>Atenção!</b> Essas informações serão exibidas em seu web site.</p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </form>


        </div>

    </div>
</div>

<nav class="navbar-footer" role="navigation">
    <div class="container">
        <div class="col-md-12 text-right">
            <a class="btn btn-danger btn-sm" href="<?=panel_anchor('dashboard')?>"><i class="fa fa-trash-o"></i>&nbsp; Cancelar</a>
            <a class="btn btn-primary" href="#" id="save"><i class="fa fa-hdd-o"></i>&nbsp; Salvar</a>
        </div>
    </div>
</nav>