
    <?php
    /** @var $alerts TYPE_NAME */
    if(!empty($error_message)): ?>
        <div class="alert alert-danger text-center" style="margin-top: -10px;">
            <i class="icon-warning-sign"></i> <?= $error_message; ?>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        </div>
    <?php endif;?>
    <?php
    if(!empty($info_message)): ?>
        <div class="alert alert-info text-center" style="margin-top: -10px;">
            <i class="icon-info-sign"></i> <?= $info_message; ?>
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
        </div>
    <?php endif;?>
    <?php
    if(!empty($success_message)): ?>
        <div class="alert alert-success text-center" style="margin-top: -10px;">
            <i class="icon-info-sign"></i> <?= htmlspecialchars_decode($success_message); ?>
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
        </div>
    <?php endif;?>
