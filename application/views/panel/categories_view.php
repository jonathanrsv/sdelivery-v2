<div class="navbar-second">
    <div class="container">
        <div class="row">
            <div class="text-left col-md-6">
                <ol class="breadcrumb"><li><a href="<?= panel_anchor(); ?>">Panel</a></li><li>loja online</li><li class="active">Categorias</li></ol>
            </div>
            <div class="text-right col-md-6">
                <a class="btn btn-primary simple-insert-categories" data-page="<?=$page_ref?>" data-id-group="1" href="#"><i class="fa fa-plus"></i>&nbsp; Adicionar categoria</a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-filter"></i> Filtro
                </div>
                <div class="panel-body">



                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-reorder"></i> Categorias Cadastradas
                </div>
                <div class="panel-body">
                    <?php //dump($clientes)?>
                    <table class="table table-striped table-responsive table-hover table-centered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Produtos na categoria</th>
                            <th class="text-center">Ação</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($categories as $key => $value): ?>
                            <tr>
                                <td><?=$value->id_categoria?></td>
                                <td width="50%"><?=$value->nome?></td>
                                <td><?=$this->products->count_by_category($value->id_categoria)?></td>
                                <td class="text-center"><a class="btn btn-danger btn-sm remove" data-page="rates" data-id="7" href="<?=panel_anchor("$page_ref/remove/$value->id_categoria")?>"><i class="fa fa-trash-o"></i>&nbsp; Remover</a></td>
                            </tr>
                        <?php endforeach ?>


                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
</div>