
<div class="navbar-second">
    <div class="container">
        <div class="row">
            <div class="text-left col-md-6">
                <ol class="breadcrumb"><li><a href="<?= panel_anchor(); ?>">Panel</a></li><li>Cliente</li><li class="active">Editar</li></ol>
            </div>
            <div class="text-right col-md-6">
                <a class="btn btn-primary simple-insert" data-page="algo>" data-id-group="<?php //echo $group->id?>" href="#"><i class="fa fa-plus"></i>&nbsp; Adicionar algo</a>
            </div>
        </div>
    </div>
</div>
<div class="container">

    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-filter"></i> Ações
                </div>
                <div class="panel-body">
                  <div class="btn-group btn-group-justified" role="group" aria-label="...">
                          <span class="btn btn-danger btn-file"> Bloquear  </span>
                  </div>
                </div>
            </div>
        </div>

        <div class="col-md-9">

                <div class="form-horizontal">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-edit"></i>&nbsp; Editar o Cliente
                    </div>
                    <div class="panel-body">
                      <div class="form-group">
                          <label for="inputUser" class="col-sm-3 control-label"> Código </span></label>
                          <div class="col-md-1">
                              <input type="hidden" name="id_cliente" value="<?=$customer->id_cliente?>" >
                              <input type="text" value="<?=$customer->id_cliente?>" class="form-control"  disabled>
                          </div>
                      </div>

                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> Cliente <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" value="<?=$customer->nome?>" class="form-control"  disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> E-mail <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" value="<?=$customer->email?>" class="form-control"  disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> CPF <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" value="<?=$customer->cpf?>" class="form-control"  disabled>
                            </div>
                        </div>






                        <div class="form-group">
                            <div class="bs-callout bs-callout-warning">
                                <p> <i class="fa fa-warning"></i>&nbsp; <b>Atenção!</b> algma bllb.</p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </form>


        </div>

    </div>
</div>

<nav class="navbar-footer" role="navigation">
    <div class="container">
        <div class="col-md-12 text-right">
            <a class="btn btn-danger btn-sm" href="<?=panel_anchor('dashboard')?>"><i class="fa fa-trash-o"></i>&nbsp; Cancelar</a>
            <a class="btn btn-primary" href="#" id="save"><i class="fa fa-hdd-o"></i>&nbsp; Salvar</a>
        </div>
    </div>
</nav>
