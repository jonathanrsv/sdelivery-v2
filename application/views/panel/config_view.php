<div class="navbar-second">
    <div class="container">
        <div class="row">
            <div class="text-left col-md-6">
                <ol class="breadcrumb"><li><a href="<?= panel_anchor(); ?>">Panel</a></li><li class="active"> Config </li> </ol>
            </div>
            <div class="text-right col-md-6">
                <a class="btn btn-primary simple-insert" data-page="algo>" data-id-group="<?php //echo $group->id?>" href="#"><i class="fa fa-plus"></i>&nbsp; Adicionar algo</a>
            </div>
        </div>
    </div>
</div>
<div class="container">

    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-filter"></i> Filtro template
                </div>
                <div class="panel-body">


                </div>
            </div>
        </div>

        <div class="col-md-9">
            <form id="submitThis" method="post" action="<?=panel_anchor("$page_ref/update/")?>" class="form-horizontal" role="form">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-cog"></i>&nbsp; Configurações da Loja Online
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> Nome Loja <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" name="nome_loja" value="<?= $this->options->get('nome_loja')->option_valor?>" class="form-control" id="inputUser" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> E-mail para contato <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" name="email_contato" value="<?= $this->options->get('email_contato')->option_valor?>" class="form-control" id="inputUser" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> Telefone <span class="required">*</span></label>
                            <div class="col-md-3">
                                <input type="text" name="telefone_contato" value="<?= $this->options->get('telefone_contato')->option_valor?>" class="form-control" id="inputUser" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> Descrição </label>
                            <div class="col-md-6">
                                <input type="text" name="descricao_loja" value="<?= $this->options->get('descricao_loja')->option_valor?>" class="form-control" id="inputUser" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="bs-callout bs-callout-warning">
                                <p> <i class="fa fa-warning"></i>&nbsp; <b>Atenção!</b> Essas informações serão exibidas em seu web site.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-cog"></i>&nbsp; Configurações de funcionamento
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> CEP Loja <span class="required">*</span></label>
                            <div class="col-md-6">
                                <input type="text" name="cep_cliente" value="<?= $this->options->get('cep_cliente')->option_valor?>" class="form-control" id="inputUser" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> Hora inicial <span class="required">*</span></label>
                            <div class="col-md-2">
                                <input type="text" name="hora_inicial" value="<?= $this->options->get('hora_inicial')->option_valor?>" class="form-control" id="inputUser" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> Hora Final <span class="required">*</span></label>
                            <div class="col-md-2">
                                <input type="text" name="hora_final" value="<?= $this->options->get('hora_final')->option_valor?>" class="form-control" id="inputUser" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> Limite de entrega <span class="required">*</span></label>
                            <div class="col-md-2">
                                <input type="text" name="limite_entrega" value="<?= $this->options->get('limite_entrega')->option_valor?>" class="form-control" id="inputUser" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputUser" class="col-sm-3 control-label"> Frete <span class="required">*</span></label>
                            <div class="col-md-2">
                                <input type="text" name="frete" value="<?= $this->options->get('frete')->option_valor?>" class="form-control" id="inputUser" placeholder="">
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="bs-callout bs-callout-danger">
                                <p> <i class="fa fa-warning"></i>&nbsp; <b>Cuidado!</b> ao alterar essas regras, o funcionamento da sua loja online será afetado.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </form>


        </div>

    </div>
</div>

<nav class="navbar-footer" role="navigation">
    <div class="container">
        <div class="col-md-12 text-right">
            <a class="btn btn-danger btn-sm" href="<?=panel_anchor('dashboard')?>"><i class="fa fa-trash-o"></i>&nbsp; Cancelar</a>
            <a class="btn btn-primary" href="#" id="save"><i class="fa fa-hdd-o"></i>&nbsp; Salvar</a>
        </div>
    </div>
</nav>