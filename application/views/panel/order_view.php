
<div class="navbar-second">
    <div class="container">
        <div class="row">
            <div class="text-left col-md-6">
                <ol class="breadcrumb"><li><a href="<?= panel_anchor(); ?>">Panel</a></li> <li>Dashboard</li> <li class="active"> Pedidos </li> </ol>
            </div>
            <div class="text-right col-md-6">
                <a class="btn btn-primary simple-insert" data-page="algo>" data-id-group="<?php //echo $group->id?>" href="#"><i class="fa fa-plus"></i>&nbsp; Adicionar algo</a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <?php if($order->status_pedido == 'Finalizado') { ?>
                <div class="alert alert-success"><strong>Atenção!</strong> Este pedindo já foi finalizado</div>
            <?php } ?>
            <?php if($order->status_pedido == 'Processando') { ?>
                <div class="alert alert-danger"><strong>Atenção!</strong> Pedindo está aguardando processamento</div>
            <?php } ?>
            <?php if($order->status_pedido == 'Cancelado') { ?>
                <div class="alert alert-info"><strong>Atenção!</strong> Este pedido foi cancelado</div>
            <?php } ?>
            <?php if($order->status_pedido == 'Recebido') { ?>
                <div class="alert alert-info"><strong>Atenção!</strong> Este pedido está aguardando processamento</div>
            <?php } ?>




        </div>

        <!--        RIGHT SIDE-->

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-info-circle"></i> Informações do Pedido
                </div>
                <div class="panel-body">

                    <table class="table table-condensed">

                        <tbody>
                        <tr>
                            <td>Codigo do Pedido</td>
                            <td><strong> <?=$order->id_pedido?> </strong></td>
                        </tr>
                        <tr>
                            <td>Data</td>
                            <td><strong><?=date('d/m/Y H:i:s', strtotime($order->data_pedido)); ?></strong></td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td>
                                <div class="btn-toolbar" role="toolbar">
                                    <div class="btn-group">
                                        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
                                            Status de Pedido <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <?php  $query = $this->db->get('tb_status_pedido'); ?>
                                            <?php foreach ($query->result() as $key => $value): ?>
                                            <li><a href="<?=panel_anchor("{$page_ref}/alterar_status?pedido={$order->id_pedido}&status={$value->id_status_pedido}")?>"><?=$value->nome?></a></li>
                                            <?php endforeach ?>
                                        </ul>
                                    </div><!-- /btn-group -->
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-user"></i> Dados do cliente
                </div>
                <div class="panel-body">
                    <table class="table table-condensed">
                        <tbody>
                        <tr>
                            <td>Nome do Cliente</td>
                            <td><strong><?=$order->nome_cliente?></strong></td>
                        </tr>
                        <tr>
                            <td>E-mail</td>
                            <td><strong><?=$order->email?></strong></td>
                        </tr>
                        <tr>
                            <td>Telefones de Contato</td>
                            <td><strong><?=$order->telefone?></strong></td>
                        </tr>
                        <tr>
                            <td>Entrega</td>
                            <td><?=$order->rua.' nº '.$order->n_endereco.' - '.$order->complemento.' - '.$order->referencia?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php if($log_pedido) { ?>
        <div class="row" style="margin-bottom: 26px;">
            <div class="col-md-12">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    <i class="fa fa-history"></i>  Clique para ver o Histórico de status de pedido
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                <?php foreach ($log_pedido as $key => $log): ?>
                                    <div class="alert alert-info"><strong><?=date('d/m/Y H:i:s', strtotime($log->created)); ?></strong> Status do pedido alerado para <strong><?=$log->status?></strong></div>
                                <?php endforeach ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-reorder"></i> Itens do Pedido
                </div>
                <div class="panel-body">
                    <table class="table table-condensed table-responsive table-hover table-centered">
                        <thead>
                        <tr>
                            <th>Produto</th>
                            <th>Preço</th>
                            <th style="text-align: center;">Qtd</th>
                            <th>Sub-Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($order_itens as $key => $value): ?>
                            <tr>
                                <td><?=$value->nome?></td>
                                <td>R$<?=$value->valor_unitario?></td>
                                <td class="text-center"><?=$value->qtd?></td>
                                <td><?=formataValor($value->valor_unitario * $value->qtd)?></td>
                            </tr>
                        <?php endforeach ?>
                        <tr class="active">
                            <td rowspan="4"><strong>Fechamento do Pedido</strong></td>
                            <td></td>
                            <td class="text-right"><strong>Total de itens</strong></td>
                            <td class="text-center"><?=formataValor($this->order_itens->sum_order()->total);?></td>
                        </tr>
                        <tr class="active">
                            <td></td>
                            <td  class="text-right"><strong>Frete</strong></td>
                            <td class="text-center"><?=formataValor($order->frete);?></td>
                        </tr>
                        <tr class="active">
                            <td></td>
                            <td  class="text-right"><strong>Total do Pedido</strong></td>
                            <td class="text-center"><?=formataValor($this->order_itens->sum_order()->total + $order->frete);?></td>
                        </tr>
                        <tr class="active">
                            <td></td>
                            <td  class="text-right"><strong>Forma de pagamento</strong></td>
                            <td class="text-center"><?=$order->form_pagamento;?></td>
                        </tr>


                        </tbody>
                    </table>



                </div>


            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-comments-o"></i> Observação do cliente
        </div>
        <div class="panel-body">
            <?=$order->obs?>
        </div>
    </div>



</div>













