<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php echo $title ." | ". $this->config->item('project_name'); ?></title>

    <!-- Core CSS - Include with every page -->
    <?php echo link_tag('public/assets/css/vendor/bootstrap.min.css'); ?>

    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->


</head>

<body>

    <div class="container" style="margin-top: 150px;">
        <div class="row">
            
            <div class="col-md-4 col-md-offset-4">
               <?php include 'alerts.php'; ?>
                <div class="login-panel panel panel-default">
                     
                    <div class="panel-heading">
                        <h3 class="panel-title">Acesso | <?php  echo $this->config->item('project_name'); ?></h3>
                    </div>
                    <div class="panel-body">

                        <form role="form" action="<?php echo base_url('panel/login') ?>" method="POST">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Digite seu usuario" name="username" value="<?php echo set_value('username'); ?>" autofocus >
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Digite sua senha" name="password" type="password" value="">
                                </div>
                                
                                <!-- Change this to a button or input when using this as a form -->
                                <input type="submit" class="btn btn-lg btn-success btn-block" value="Entrar">
                            </fieldset>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Core Scripts - Include with every page -->
    <script src="<?php echo base_url('/public/assets/js/vendor/jquery-1.10.2.min.js'); ?>"></script>
    <script src="<?php echo base_url('public/assets/js/vendor/bootstrap.min.js'); ?>"></script>



</body>

</html>
