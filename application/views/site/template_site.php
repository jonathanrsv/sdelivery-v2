<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="UTF-8">
<title><?=$title . ' | '. $this->options->get('nome_loja')->option_valor?></title>

<meta name="description" content="Chef Delivery">

<?php echo link_tag('public/assets/css/vendor/bootstrap.min.css'); ?>
<?php echo link_tag('public/assets/css/site.css'); ?>
<?php echo link_tag('public/assets/css/vendor/jquery.bxslider.css'); ?>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">


<link href='//fonts.googleapis.com/css?family=Roboto:400,700,300&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>

<?php echo link_tag('public/assets/css/vendor/product_slider.css'); ?>
<?php echo link_tag('public/assets/css/vendor/product_tabs.css'); ?>
<?php echo link_tag('public/assets/css/vendor/carousel.css'); ?>
<?php echo link_tag('public/assets/css/vendor/jquery.awesome-cropper.css'); ?>
<?php echo link_tag('public/assets/css/delivery_template.css')?>


<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>

<?php
    echo '<script type="text/javascript">';
    echo 'var digitalData = ';
    echo json_encode($digitalData);
    echo '';
    echo '</script>';
?>
<script src="//assets.adobedtm.com/02480ae9ffaf06c66de52245a702f3abf28a0ffc/satelliteLib-7b10079d3daef50b17151dcb5a3413f19b54a220.js"></script>
</head>
<body>

<div class="wrap">
<!-- top panel -->
<div class="navbar navbar-inverse navbar-fixed-top topbar">
    <div class="container">

        <!-- cart -->
        <div id="cart">
            <div class="heading">
                <a href="<?=site_anchor('carrinho/view')?>" title="Carrinho de compras">
           <span id="cart-total">
               <i class="fa fa-shopping-cart fa-lg"></i>&nbsp;
               <span class="hidden-xs"> <?=$this->cart->total_items();?> item(s) - <?=formataValor($this->cart->total())?></span>
           </span>
                </a>
            </div>

            <div class="content">
                <div class="inner">
                    <div class="mini-cart-info">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td class="image">
                                    <a href="<?=site_anchor()?>">
                                        <img src="http://burger.erpshop.ru/image/cache/data-menu-sandwiches-caesar-salad-60x60.jpg" alt="Caesar" title="Caesar">
                                    </a>
                                </td>
                                <td class="name">
                                    <a href="http://burger.erpshop.ru/appetizer/caesar">
                                        Caesar                            </a>
                                    <div>
                                    </div>
                                </td>
                                <td class="quantity">x&nbsp;1</td>
                                <td class="total">120.00 р.</td>
                                <td class="remove">
                                    <button type="button" class="close" aria-hidden="true" title="Remove" onclick="(getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') ? location = 'index.php?route=checkout/cart&remove=6869' : $('#cart').load('index.php?route=module/cart&remove=6869' + ' #cart > *');">&times;</button>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="mini-cart-total">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td class="name"><b>Sub-Total:</b></td>
                                <td class="total">120.00 р.</td>
                            </tr>
                            <tr>
                                <td class="name"><b>Total:</b></td>
                                <td class="total">120.00 р.</td>
                            </tr>
                        </table>
                    </div>
                    <div class="checkout">
                        <a class="btn btn-sm btn-success" href="http://burger.erpshop.ru/shopping-cart/">
                            View Cart                </a>
                        <a class="btn btn-sm btn-success" href="http://burger.erpshop.ru/checkout/">
                            Checkout                </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="divider"></div>


        <!-- menu -->
        <div id="menu" class="menu-container">
            <button type="button" class="menu-toggle" data-toggle="collapse" data-target="#menu-mobile">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <div class="collapse menu-collapse" id="menu-mobile">
                <ul class="nav nav-pills">
                    <?php foreach ($categories as $key => $categorie): ?>
                        <li>
                            <a href="<?=site_anchor("categoria/$categorie->slug")?>"><?=$categorie->nome?></a>
                        </li>
                    <?php endforeach ?>

                </ul>
            </div>
        </div>
        <div class="divider hidden-sm hidden-xs"></div>

    </div>
</div>
<!-- //top panel -->

<!-- header -->
<div class="header">
    <div class="container">

        <div class="row">

            <div class="col-sm-4">
                <div id="logo">
                    <a href="<?=site_anchor()?>"><img src="http://burger.erpshop.ru/image/data/logo-3.png" title="Chef Delivery" alt="Chef Delivery" /></a>
                </div>
            </div>

            <div class="col-sm-8">
                <div id="search">
                    <div class="input-group">
                        <input type="text" name="s"id="stringSearch" class="form-control" placeholder="Busca" value="">
                                <span class="input-group-btn">                                    
                                    <button class="btn btn-default button-search" id="button-search" type="button"><span class="fa fa-search"></span></button>
                                </span>
                    </div>
                </div>

                <div class="header-right">
                    <?php if (!$this->login->isLogged()) { ?>
                        <div id="welcome">
                          Bem-vindo, faça seu login ou <a href="<?=site_anchor('cadastro')?>">clique aqui para se cadastrar</a>.
                        </div>
                        <form id="login_rapido" action="<?=site_anchor('logar')?>" method="POST">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>E-mail</label>
                                <input type="text" class="form-control" data-validation="email" data-validation-error-msg="Digite um e-mail válido" name="username" value="">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label>Senha</label>
                                <input type="password" data-validation="required" data-validation-error-msg="Senha obrigatória" class="form-control" name="password" value="">
                            </div>

                        </div>
                        <input type="submit" value="Login" id="button-login" class="btn btn-warning" style="margin-top: 25px;">
                        </form>
                    <?php } ?>

                    <?php if ($this->login->isLogged()) { ?>
                        <div id="welcome">
                            Bem-vindo <?=$this->session->userdata('nome')?>, faça seu pedidos</a>.
                        </div>
                        <div class="col-sm-2">
                            <a href="<?=site_anchor('pedidos')?>">
                                <i class="fa fa-list-ul fa-5x"></i>
                                <div style="margin-left: 12px;">Pedidos</div>
                            </a>
                        </div>
                        <div class="col-sm-2">
                            <a href="<?=site_anchor('cadastro/endereco')?>">
                                <i class="fa fa-plus fa-5x"></i>
                                <div style="margin-left: -6px;">Endereços</div>
                            </a>
                        </div>
                        <div class="col-sm-2">
                            <a href="<?=site_anchor('sair')?>">
                                <i class="fa fa-power-off fa-5x"></i>
                                <div style="margin-left: 14px;">Sair</div>
                            </a>
                        </div>


                    <?php } ?>



                </div>
            </div>
        </div>
    </div>
</div>
<!-- //header -->

<div id="container" class="content">
<div class="container">
<div id="notification">
    <?php include 'alerts.php'; ?>
</div>
<?=$contents?>
</div>
</div>
<div id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="column">
                    <h4>Informações</h4>
                    <ul>                        
                        <li>
                            <a href="#">Sobre Nós</a>
                        </li>
                        <li>
                            <a href="#">Política De Privacidade</a>
                        </li>
                        <li>
                            <a href="#">Termos e condiçõe</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="column">
                    <h4>Atendimento Ao Cliente</h4>
                    <ul>
                        <li><a href="#">Contato</a></li>
                        <li><a href="#">Mapa do site</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="column">
                    <h4>Minha conta</h4>
                    <ul>
                        <li><a href="#">Minha Conta</a></li>
                        <li><a href="#">Histórico de pedidos</a></li>
                        <li><a href="#">Lista de desejos</a></li>
                        <li><a href="#">Newsletter</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-sm-9 ">
                <div id="about">
                    <h3>sDelivery</h3>
                </div>
            </div>

        </div>
    </div>
</div>
<script src="<?php echo base_url('public/assets/js/digitalValidator.js'); ?>" id='validator'></script>
<script src="<?php echo base_url('public/assets/js/vendor/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/js/vendor/jquery.bxslider.min.js'); ?>"></script>
<script src="<?php echo base_url('public/assets/js/site.js'); ?>"></script>


<script>
$('#validator').load(function () { 
    digitalValidator.sendRequest();
}); 

$.formUtils.addValidator({
    name : 'cpf',
    validatorFunction : function(value, $el, config, language, $form) {
        var numeros, digitos, soma, i, resultado, digitos_iguais = 1;
        value = value.toString().replace(/[^\d]+/g,'');
        var cpf_length = value.length;

        if (cpf_length < 11) return '1';

        for (i = 0; i < cpf_length - 1; i++){
            if (value.charAt(i) != value.charAt(i + 1)){
                digitos_iguais = 0;
                break;
            }
        }

        if (digitos_iguais) return '2';

        numeros = value.substring(0,9);
        digitos = value.substring(9);
        soma = 0;

        for (i = 10; i > 1; i--){
            soma += numeros.charAt(10 - i) * i;
        }

        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

        if (resultado != digitos.charAt(0)) return false;

        numeros = value.substring(0,10);
        soma = 0;
        for (i = 11; i > 1; i--){
            soma += numeros.charAt(11 - i) * i;
        }

        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;

        if (resultado != digitos.charAt(1)) return false;
        
        return true;
    },
    errorMessage : 'Digite um CPF válido',
    errorMessageKey: 'badEvenNumber'
});
$.validate();
    
</script>
<script type="text/javascript">_satellite.pageBottom();</script>

</body>
</html>