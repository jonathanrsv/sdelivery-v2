<div id="container" class="content">
    <div class="container">
        <div id="notification"></div> 
        <div id="column-right" class="hidden-xs hidden-sm">
            <div class="box">
                <div class="box-heading">
                    <span>Categorias</span>
                </div>
                <div class="box-content">
                    <ul class="box-category treemenu">
                        <?php foreach ($categories as $key => $categorie): ?>

                        <li>
                            <a href="<?=site_anchor("categoria/$categorie->slug")?>" <?=$this->uri->segment(2) == ($categorie->slug) ? 'class="active"' : '' ;?>>
                                <span><?=$categorie->nome?> (<?=$this->products->count_by_category($categorie->id_categoria)?>)</span>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>



    </div>
    <div id="content">
        <ol class="breadcrumb">
            <li>
                <a href="#">
                    Home            </a>
                </li>
                <li>
                    <a href="#">
                        <?=$this->categories->find($product->id_categoria)->nome; ?>            </a>
                    </li>
                    <li>
                        <a href="#">
                         <?=$product->nome?>          </a>
                     </li>
                 </ol>

                 <div class="product-info">
                    <div class="row">
                        <div class="col-md-4 popup-gallery">
                            <div class="image">
                                <a href="#" title="Сheeseburger" class="imagebox">
                                    <img src="<?=site_anchor('public/uploads/' . $product->imagem) ?> " title="Сheeseburger" alt="Сheeseburger" id="image">
                                </a>
                            </div>


                            
                        </div>

                        <div class="col-md-8 product-center clearfix">
                            <h1> <?=$product->nome?>   </h1>
                            <div class="description">

                                
                                <strong>Código do Produto:</strong>  <?=$product->id_produto?>                   <br>


                                <strong>Disponibilidade:</strong>  <?=$product->qtd_estoque?>               </div>

                                <div class="options">
                                 

                                </div>
                            </div>
                        </div>



                        <div class="cart-area">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="review">
                                        <div class="stars" title="0 reviews">
                                        </div>

                                        
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="price">
                                        <div>
                                            <span class="price-fixed"> R$ <?=$product->valor?></span>
                                        </div>
                                    </div>

                                    <div class="text-center" style="font-size: 12px">


                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="cart">
                                        <div class="add-to-cart clearfix" style="margin-top: 20px">
                                            <!--span class="help-block">Qty:</span-->
                                            <div class="input-group input-group-lg">
                                                <input type="text" name="quantity" class="form-control" size="2" value="1">
                                                <span class="input-group-btn">
                                                    <button type="button" id="button-cart" class="btn btn-success">
                                                        <i class="fa fa-shopping-cart"></i> &nbsp;
                                                        <a href="<?=site_anchor("carrinho/add/". $product->id_produto)?>">Comprar   </a>                               </button>
                                                    </span>
                                                </div>
                                                <input type="hidden" name="product_id" size="2" value="6865">
                                            </div>

                                            <div class="links">
                                              
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="product-tabs">
                            <ul class="nav nav-tabs" id="tabs">
                                <li class="active"><a href="#tab-description" data-toggle="tab">Descrição</a></li>

                                
                            </ul>

                            <div class="tab-content">
                                <div id="tab-description" class="tab-pane fade active in">
                                    <p>
                                     <?=$product->descricao?></p>
                                 </div>

                                 

                                 
                             </div>
                         </div>



                         <div class="box box-latest">
                            <div class="box-heading">
                                <span>Produtos do seu interesse</span>
                            </div>
                            <div class="box-content">
                                <div class="box-product product-grid">
                                    <?php foreach ($products as $key => $product): ?>   
                                    <div class="item">
                                        <div class="image">
                                            <a href="<?=site_anchor('produto/' . $product->slug_produto)?>">
                                                <img src="<?=site_anchor("public/uploads/$product->imagem")?>" title="<?=$product->nome?>" alt="<?=$product->nome?>">
                                            </a>
                                        </div>
                                        <div class="caption">
                                            <div class="name">
                                                <a href="<?=site_anchor('produto/' . $product->slug_produto)?>"><?=$product->nome?></a>
                                            </div>

                                            <div class="price">
                                                <div>
                                                    <span class="price-fixed">240.00 р.</span>
                                                </div>
                                            </div>


                                            <div class="cart">
                                                <button class="btn btn-success" type="button" onclick="location.href='<?=site_anchor("carrinho/add/{$product->id_produto}")?>'">
                                                    <i class="fa fa-shopping-cart"></i> <span>Comprar</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                                
                            </div>
                        </div>
                    </div>
                </div>





            </div>
        </div>