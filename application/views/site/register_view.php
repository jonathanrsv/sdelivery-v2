<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Formulário de Cadastro</h3>
  </div>
  <div class="panel-body">
      <form class="form-horizontal" method="post" action="<?=site_anchor("cadastro/save")?>">
      <fieldset>

      <!-- Form Name -->
      

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="nome">Nome</label>  
        <div class="col-md-6">
        <input id="nome" data-validation="required" data-validation-error-msg="Nome obrigatório" name="nome" type="text" placeholder="Digite seu nome" class="form-control input-md">
          
        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="email">E-mail</label>  
        <div class="col-md-5">
        <input id="email" name="username" type="text" data-validation="email" data-validation-error-msg="Digite um e-mail válido" placeholder="exemplo@exemplo.com" class="form-control input-md">
          
        </div>
      </div>

      <!-- Password input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="senha">Senha</label>
        <div class="col-md-4">
          <input id="senha" name="password" data-validation="required" data-validation-error-msg="Senha obrigatória" type="password" placeholder="" class="form-control input-md">
          
        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="cpf">CPF</label>  
        <div class="col-md-4">
        <input id="cpf" name="cpf" data-validation="cpf" data-validation-error-msg="Digite um CPF válido" type="text" placeholder="Digite seu CPF" class="form-control input-md">
          
        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="datanasc">Data de Nascimento</label>  
        <div class="col-md-4">
        <input id="datanasc" name="datanasc" data-validation="date" data-validation-error-msg="Digite a data de nascimento" data-validation-format="dd/mm/yyyy" type="text" placeholder="dd/mm/aaaa" class="form-control input-md">
          
        </div>
      </div>

      <!-- Button -->
      <div class="form-group">
        <label class="col-md-4 control-label" for=""></label>
        <div class="col-md-4">
          <button id="" name="" class="btn btn-primary">Cadastrar</button>
        </div>
      </div>

      <legend></legend>
      </form>
  </div>
</div>