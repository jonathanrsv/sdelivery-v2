<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Cadastro de endereço</h3>
  </div>
  <div class="panel-body">

    <form class="form-horizontal" method="post" action="<?=site_anchor("cadastro/endereco")?>">
      <fieldset>


      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="cep">Digite seu CEP</label>
        <div class="col-md-4">
        <input id="cep" name="cep" type="text" placeholder="00000-000" class="form-control input-md">

        </div>
      </div>

      <div id="ocultaForm" style="display:none">

         <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="endereco">Endereço</label>
        <div class="col-md-6">
        <input id="endereco" name="endereco" type="text" placeholder="Rua exemplo" class="form-control input-md">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="numero">Numero</label>
        <div class="col-md-2">
        <input id="numero" name="numero" type="text" placeholder="" class="form-control input-md">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="Bairro">Bairro</label>
        <div class="col-md-4">
        <input id="bairro" name="bairro" type="text" placeholder="" class="form-control input-md">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="cidade">Cidade</label>
        <div class="col-md-4">
        <input id="cidade" name="cidade" type="text" placeholder="" class="form-control input-md">

        </div>
      </div>

       <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="cidade">Estado</label>
        <div class="col-md-4">
        <input id="estado" name="estado" type="text" placeholder="" class="form-control input-md">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="referencia">Referencia</label>
        <div class="col-md-5">
        <input id="referencia" name="referencia" type="text" placeholder="Prox ao mercado" class="form-control input-md">

        </div>
      </div>

      <!-- Text input-->
      <div class="form-group">
        <label class="col-md-4 control-label" for="referencia">Complemento</label>
        <div class="col-md-5">
        <input id="complemento" name="complemento" type="text" placeholder="2º andar" class="form-control input-md">

        </div>
      </div>

      <!-- Button -->
      <div class="form-group">
        <label class="col-md-4 control-label" for=""></label>
        <div class="col-md-4">
          <button id="" name="" class="btn btn-primary">Cadastrar</button>
        </div>
      </div>

      </div>



      </fieldset>
      </form>



  </div>
</div>


<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Seus endereços</h3>
    </div>
    <div class="panel-body">
        <?//=dump($client_address)?>

        <div class="content">
            <?php foreach ($client_address as $address) { ?>

                <div class="radio">
                    <label>
                      <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                        <?=$address->endereco ?>,  <?=$address->numero ?> - <?=$address->complemento ?> - <?=$address->bairro?> - CEP <?=$address->cep ?> - <?=$address->estado ?>
                    </label>
                </div>

            <?php } ?>

        </div>

    </div>
    <div class="panel-footer">
        <div class="buttons">
            <div class="text-left">


            </div>
        </div>
    </div>

</div>
