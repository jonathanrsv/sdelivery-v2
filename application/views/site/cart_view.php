<div class="page-header">
    <h1>
        <?=$heading?><small>&nbsp;(<?=formataValor($this->cart->total())?>)</small>
    </h1>
</div>
<form action="<?=site_anchor('carrinho/update')?>" method="post" enctype="multipart/form-data">
    <div class="cart-info table-responsive">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th class="image">Imagem</th>
                <th class="name">Produto</th>
                <th class="quantity">Quantidade</th>
                <th class="price">Preço uni.</th>
                <th class="total">Total</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($cart_itens as $items): ?>
                <tr>
                    <td class="image">
                        <a href="#"><img src="<?=site_anchor("public/uploads/{$items['imagem']}")?>"  style="width: 60px;" alt="Traditional" title="Traditional"></a>
                    </td>
                    <td class="name">

                        <a href="#"><?=$items['name']?></a>
                        <div>
                        </div>
                    </td>

                    <td class="quantity">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button class="btn btn-success" type="image" alt="Update" title="Update"><i class="fa fa-refresh"></i></button>
                            </span>

                            <input class="form-control" type="text" name="qty[<?=$items['rowid']?>]" value="<?=$items['qty']?>" size="1">
                            <span class="input-group-btn">
                                <a class="btn btn-danger" href="<?=site_anchor("carrinho/remove/{$items['rowid']}")?>" title="Remove">
                                    <i class="fa fa-times"></i>
                                </a>
                            </span>
                        </div>
                    </td>
                    <td class="price"><?=formataValor($items['price'])?></td>
                    <td class="total"><?=formataValor($items['subtotal'])?></td>
                </tr>

            <?php endforeach ?>

            </tbody>
        </table>
        <div class="buttons">
            <div class="text-right">

                <a href="<?=site_anchor('carrinho/endereco')?>" class="btn btn-warning">
                   Finalizar a compra <i class="fa fa-long-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
</form>
