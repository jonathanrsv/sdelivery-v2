<div class="container">
    <div id="notification"></div>
    <div id="column-left" class="hidden-xs hidden-sm">
        <div class="box">
            <div class="box-heading">
                <span>Categorias</span>
            </div>
            <div class="box-content">
                <ul class="box-category treemenu">
                    <?php foreach ($categories as $key => $categorie): ?>

                        <li>
                            <a href="<?=site_anchor("categoria/$categorie->slug")?>" <?=$this->uri->segment(2) == ($categorie->slug) ? 'class="active"' : '' ;?>>
                                <span><?=$categorie->nome?> (<?=$this->products->count_by_category($categorie->id_categoria)?>)</span>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    </div>

    <div id="content">
        <div class="category-info clearfix">
            <h1><?=$heading?></h1>
        </div>
        <div class="product-grid box-product">
             <?php
            if(empty($products))
            {
                echo "Não existem produtos para a sua busca :(";
            }
            ?>
            <?php foreach ($products as $key => $product): ?>
                <div class="item">
                    <div class="image">
                        <a href="<?=site_anchor('produto/'. $product->slug_produto)?>">
                            <img src="<?=site_anchor("public/uploads/{$product->imagem}")?>" title="<?=$product->nome?>" alt="<?=$product->nome?>">
                        </a>
                    </div>
                    <div class="caption">
                        <div class="name">
                            <a href="<?=site_anchor('produto/'. $product->slug_produto)?>"><?=$product->nome?></a>
                        </div>
                        <div class="description"><?=$product->descricao?> </div>

                        <div class="price">
                            <div>
                                <span class="price-fixed"><?=formataValor($product->valor)?></span>
                            </div>
                        </div>


                        <div class="cart">
                            <button class="btn btn-success" type="button" onclick="location.href='<?=site_anchor("carrinho/add/{$product->id_produto}")?>'" >
                                <i class="fa fa-shopping-cart"></i> <span>Comprar</span>
                            </button>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>