<div class="page-header">
    <h1>
        <?=$heading?><small>&nbsp;(<?=formataValor($this->cart->total())?>)</small>
    </h1>
</div>
<form action="#" method="post" enctype="multipart/form-data" id="submitThis">
    <div class="cart-info table-responsive">


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Selecione abaixo um metodo de pagamento</h3>
            </div>
            <div class="panel-body">
                <?//=dump($client_address)?>

                <div class="content">
                    <?php foreach ($pagamentos as $payment) { ?>

                        <div class="radio">
                            <label>
                            <input type="radio" name="id_formapag" value="<?=$payment->id_formapag ?>" >
                              <?=$payment->nome ?>
                            </label>
                        </div>

                    <?php } ?>

                </div>

            </div>
            <div class="panel-footer">
                <div class="buttons">
                    <div class="text-left">

                       
                    </div>
                </div>
            </div>

        </div>

        <div class="buttons">
            <div class="text-right">

                <a href="<?=site_anchor('carrinho/confirmacao')?>" id="save" class="btn btn-warning">
                    Confirmar a compra <i class="fa fa-long-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
</form>
