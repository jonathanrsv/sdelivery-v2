<div id="content">
    <div class="box">

        <ul id="product-tabs-0" class="nav nav-tabs">
            <li class="active">
                <a href="#tab-destaque" data-toggle="tab">
                    <span>Destaque</span>
                </a>
            </li>
            <li>
                <a href="#tab-mais-vendidos" data-toggle="tab">
                    <span>Mais Vendidos</span>
                </a>
            </li>
            <li>
                <a href="#tab-novidades" data-toggle="tab">
                    <span>Novidades</span>
                </a>
            </li>
        </ul>


        <div class="box-content tab-content" id="product-slideshow0">


            <div class="box-product product-grid tab-pane fade in active" id="tab-destaque">
                <!-- Begin box-product div -->
                <?php foreach ($products as $key => $product): ?>
                    <div class="item">
                        <div class="image">
                            <a href="<?=site_anchor('produto/' . $product->slug_produto)?>">
                                <img src="<?=site_anchor("public/uploads/$product->imagem")?>" alt="Сheeseburger" width="240" height="240">
                            </a>
                        </div>

                        <div class="caption">
                            <div class="name">
                                <a href="<?=site_anchor('produto/' . $product->slug_produto)?>"><?=$product->nome?></a>
                            </div>

                            <div class="price">
                                <div>
                                    <span class="price-fixed">R$<?=$product->valor?></span>
                                </div>
                            </div>


                            <div class="cart">
                                <button type="button" onclick="location.href='<?=site_anchor("carrinho/add/{$product->id_produto}")?>'" class="btn btn-success">
                                   <i class="fa fa-shopping-cart"></i> <span>Comprar</span>
                                </button>

                            </div>
                        </div>
                    </div>
                <?php endforeach ?>



            </div>



            <div class="box-product product-grid tab-pane fade" id="tab-mais-vendidos">

            </div>


            <div class="box-product product-grid tab-pane fade" id="tab-novidades">

            </div>


        </div>
    </div>
    <div id="banner0" class="banner clearfix">
        <div class="item pull-left">
            <a href="/sandwich">
                <img src="http://burger.erpshop.ru/image/cache/data-banners-long-1100x125.jpg" alt="Sandwiches" title="Sandwiches">
            </a>
        </div>
    </div>

</div>