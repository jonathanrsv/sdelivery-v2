<div class="page-header">
    <h1>
        <?=$heading?><small>&nbsp;(<?=formataValor($this->cart->total())?>)</small>
    </h1>
</div>
<form action="#" method="post" enctype="multipart/form-data" id="submitThis">
    <div class="cart-info table-responsive">


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Selecione abaixo um endereço de entrega</h3>
            </div>
            <div class="panel-body">
                <?//=dump($client_address)?>

                <div class="content">
                    <?php foreach ($client_address as $address) { ?>

                        <div class="radio">
                            <label>
                            <input type="radio" name="id_endereco" data-cep="<?=$address->cep ?>" value="<?=$address->id_endereco ?>" >
                                <?=$address->endereco ?>,  <?=$address->numero ?> - <?=$address->complemento ?> - <?=$address->bairro?> - CEP <?=$address->cep ?> - <?=$address->estado ?>
                            </label>
                        </div>

                    <?php } ?>

                </div>

            </div>
            <div class="panel-footer">
                <div class="buttons">
                    <div class="text-left">

                        <a href="<?=site_anchor('cadastro/endereco')?>" class="btn btn-warning">
                            <i class="fa fa-plus"></i> Cadastrar endereço
                        </a>
                    </div>
                </div>
            </div>

        </div>

        <div class="buttons">
            <div class="text-right">

                <a href="<?=site_anchor('carrinho/pagamento')?>" id="save" class="btn btn-warning">
                    Finalizar a compra <i class="fa fa-long-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
</form>
