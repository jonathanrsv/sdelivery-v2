<div class="page-header">
    <h1>
        <?=$heading?><small>&nbsp;(<?=formataValor($this->cart->total())?>)</small>
    </h1>
</div>
<form action="#" method="post" enctype="multipart/form-data" id="submitThis">
    <div class="cart-info table-responsive">


        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Revisão</h3>
            </div>
            <div class="panel-body">
                <?//=dump($client_address)?>

                <div class="content">
                   <table class="table table-bordered">
                        <thead>
                        <tr>                            
                            <th class="name">Produto</th>
                            <th class="name">qtd</th>
                            <th class="price">Preço uni.</th>
                            <th class="total">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?//=dump($cart_itens)?>
                        <?php foreach ($cart_itens as $items): ?>
                            <tr>                               
                                <td class="name">

                                    <a href="#"><?=$items['name']?></a>
                                    <div>
                                    </div>
                                </td>
                                <td><?=$items['qty']?></td>
                                <td class="price"><?=formataValor($items['price'])?></td>
                                <td class="total"><?=formataValor($items['subtotal'])?></td>
                            </tr>

                        <?php endforeach ?>

                        </tbody>
                    </table>

                    <p>Metodo de pagamento: <strong><?=$payment?></strong></p>
                    <p>Total da Compra:     <strong><?=formataValor($totalCart)?></strong></p>
                    <p>Frete:               <strong>R$<?=$frete?></strong></p>
                    <p>Total Geral:         <strong><?=formataValor($total)?></strong></p>

                </div>

            </div>
            <div class="panel-footer">
                <div class="buttons">
                    <div class="text-left">

                       
                    </div>
                </div>
            </div>

        </div>

        <div class="buttons">
            <div class="text-right">

                <a href="<?=site_anchor('carrinho/finalzarCompra')?>" id="save" class="btn btn-warning">
                    Finalizar compra <i class="fa fa-long-arrow-right"></i>
                </a>
            </div>
        </div>
    </div>
</form>
