<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Digitaldata extends CI_Controller { 

    public $layer = array();

    public function __construct()
    {
    	parent::__construct();
        $this->CI =& get_instance();
    	$this->data['area'] = 'site';
        $this->load->model('login_model', 'login');
        $this->load->library('cart');
        $this->load->model('options_model', 'options');
        $this->load->model('categories_model', 'categories');       
    }

    public function mountCart($step = null)
    {    	
        $this->layer['checkout']['productsTotal']   = $this->cart->total();
        $this->layer['checkout']['quantityTotal']   = $this->cart->total_items();
        $this->layer['checkout']['shippingType']    = 'Normal';
        $this->layer['checkout']['step']            = 'shoppingCart';
        $this->layer['page']['templateName']        = 'checkout';
        $this->layer['page']['hierarchy']           = "checkout:carrinho";
        $this->layer['page']['name']                = 'bc:checkout:visualizacao';

        $arrayStepsName = [
            "address"  => "bc:checkout:endereco",
            "paymentMethod" => "bc:checkout:forma-pagamento",
            "confirmation" => "bc:checkout:confirmacao",
            "purchase" => "bc:checkout:finalizar-compra"

        ];
        $arraySteps = [
            "address"  => "endereco",
            "paymentMethod" => "forma-pagamento",
            "confirmation" => "confirmacao",
            "purchase" => "finalizar-compra"

        ];

        $items = array();
        foreach ($this->cart->contents() as $key => $value) {
            array_push($items, 
                array(
                    'idsku'     => $value['id'],
                    'quantity'  => $value['qty'],
                    'price'     => $value['price'],
                    'name'      => $value['name'],
                    'image'     => $value['imagem'],
                    'subtotal'  => $value['subtotal']

            ));
        }
        
        $this->layer['checkout']['items'] = $items;

        if($step) 
        {
            $this->layer['page']['name']     = $arrayStepsName[$step];
            $this->layer['checkout']['step'] = $arraySteps[$step];

            if($step == "purchase")
            {
                $this->layer['checkout']['deliveryZipCode'] = '08234-567';
                $this->layer['checkout']['promotionCodeID'] = '123abc';
                $this->layer['checkout']['promotionCodeValue'] = 40.25;
            }
        }

        return $this->layer;
        
    }

    public function mountDefault()
    {

    	$this->layer['page']['protocol']    = 'http:';
        $this->layer['page']['hasjQuery']   = true;
        $this->layer['page']['referrer']    = (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : '');
        $this->layer['page']['pathname']    = (uri_string() ? uri_string() : '/');

        $this->layer['site']['name']        = 'sDelivery';
        $this->layer['site']['country']     = 'BR';
        $this->layer['site']['currency']    = 'BRL';
        $this->layer['site']['domain']      = site_anchor();
        $this->layer['site']['region']      = 'pt-br';
        $this->layer['site']['test']        = false;
        $this->layer['site']['cep_origem']  = $this->options->get('cep_cliente')->option_valor;

        $this->layer['tms']['coreLocation'] = false;
        $this->layer['tms']['customLocation'] = false;
        $this->layer['tms']['digitalDataVersion'] = 28122014.1;

        if ($this->login->isLogged()) 
        {
            $this->layer['session']['user']['zipcode'] = '00000-000';
            $this->layer['session']['user']['status'] = 'logado';
            $this->layer['session']['user']['name'] = $this->session->userdata('nome');
            $this->layer['session']['user']['cpf'] = $this->session->userdata('cpf');
            $this->layer['session']['user']['dataNascimento'] = $this->session->userdata('datanasc');
            $this->layer['session']['user']['email'] = $this->session->userdata('email');
            $this->layer['session']['user']['ip_address'] = $this->session->userdata('ip_address');
        }
        return $this->layer;
    }
    public function mountPage($params = null)
    {
        if (is_array($params)) 
        {
            
            $this->layer['page'] =  array_merge($this->layer['page'], $params);
            
        }
        return $this->layer;
    }

}

?>