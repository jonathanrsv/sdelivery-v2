<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['default_controller'] = "site/site";
$route['feed'] = 'site/site/feed';
$route['site'] = 'site/site';
$route['sair'] = 'panel/login/logout';
$route['carrinho'] = 'site/cart/view';
$route['carrinho/endereco'] = 'site/cart/address';
$route['carrinho/(:any)'] = 'site/cart/$1';
$route['carrinho/add/(:any)'] = 'site/cart/add/$1';
$route['carrinho/pagamento'] = 'site/cart/paymentMethod';
$route['carrinho/confirmacao'] = 'site/cart/confirmation';
$route['carrinho/finalzarCompra'] = 'site/cart/purchase';


$route['logar'] = 'panel/login';
$route['logar_site'] = 'panel/login/site';

$route['busca'] = "site/site/search";

$route['cadastro'] = "site/site/register";
$route['cadastro/(:any)'] = "site/site/register/$1";
$route['cadastro/endereco'] = "site/site/address";
$route['cadastro/endereco/(:any)'] = "site/site/address/$1";

$route['categoria'] = "site/site/category";
$route['categoria/(:any)'] = "site/site/category/$1";

$route['produto/(:any)'] = "site/site/product/$1";

$route['panel'] = "panel/login";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */