<?php
function panel_anchor($value = null)
{
    return base_url('panel/' . $value);
}
function site_anchor($value = null)
{
    return base_url($value);
}
function slugify($text)
{
    // replace non letter or digits by -
    $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

    // trim
    $text = trim($text, '-');

    // transliterate
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

    // lowercase
    $text = strtolower($text);

    // remove unwanted characters
    $text = preg_replace('~[^-\w]+~', '', $text);

    if (empty($text)) {
        return 'n-a';
    }

    return $text;
}
function dump($expression)
{
    echo '<pre>';
        print_r($expression);
    echo '</pre>';
}

function formataValor($moeda)
{

    $moeda = number_format($moeda, 2, ',', '.');
    //$moeda = str_replace(".", ",", $moeda);
    return "R$".$moeda;
}//formataValor

function formataValorReal($value)
{
    $remove =   array("R$", ",");
    $subs   =   array("", ".");
    $result = str_replace($remove, $subs, $value);

    return $result;
}//formataValor