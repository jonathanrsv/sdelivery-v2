<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class categories extends MY_Controller_Panel
{
    public function __construct()
    {
        parent::__construct();
        $this->data['page_ref'] = 'categories';
        $this->load->model('categories_model', 'categories');
        $this->load->model('products_model', 'products');
        $this->verifyAuth();
    }

    public function index()
    {
        $this->data['title']        =   'Categorias';
        $this->data['categories']   =   $this->categories->get();
        $this->template->load('panel/template_panel_new', 'panel/categories_view', $this->data);
    }

    public function remove($id)
    {
        if(!empty($id))
        {
            if($this->categories->remove($id))
            {
                $this->session->set_flashdata('success_message', 'Categoria removida com sucesso');
                redirect(panel_anchor('categories'));
            }
            else
            {
                $this->session->set_flashdata('error_message', 'Ocorreu um erro ao remover a categoria!');
                redirect(panel_anchor('categories'));
            }
        }
        else
        {
            $this->session->set_flashdata('info_message', 'Você precisa selecionar uma categoria');
            redirect(panel_anchor('categories'));
        }

    }

    public function add()
    {
        $this->categories->add();
        $data['status'] = 'tra_ok';
        echo json_encode($data);
    }
}