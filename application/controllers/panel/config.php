<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class config extends MY_Controller_Panel
{
    public function __construct()
    {
        parent::__construct();
        $this->data['page_ref'] = 'config';
        $this->verifyAuth();
        $this->load->model('options_model', 'options');
    }

    public function index()
    {
        $this->data['title']  = 'Configurações';
        $this->template->load('panel/template_panel_new', 'panel/config_view', $this->data);
    }

    public function update()
    {
        if(!empty($_POST))
        {
            $data_config = $this->input->post();
            foreach ($data_config as $key => $value){
                $this->options->set($key, $value);
            }
            
            $this->session->set_flashdata('success_message', '<strong>Tudo certo!</strong> As configurações foram alteradas com sucesso!');
            redirect(panel_anchor('config'));
        }
        else
        {
            $this->session->set_flashdata('error_message', '<strong>Atenção!</strong> Houve um erro para alterar as configurações');
            redirect(panel_anchor('config'));
        }
    }
}