<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MY_Controller_Panel
{

    public function __construct()
    {
        parent::__construct();
        $this->verifyAuth();


        $this->data['page_ref'] = 'products';
        $this->load->model('products_model', 'products');
        $this->load->model('categories_model', 'categories');

    }

    public function index()
    {
        $this->data['title']        =   'Produtos';
        $filter = array();
        if(isset($_GET['nome']))
        {
            $filter['nome']       = $this->input->get('nome');
        }
        $this->data['produtos']     = $this->products->get($filter);
        $this->template->load('panel/template_panel_new', 'panel/products_view', $this->data);
    }
    public function edit($id)
    {
        if(!$id)
        {
            $this->session->set_flashdata('error_message', 'Por favor, selecione um produto');
            redirect(panel_anchor('products'));
            exit();
        }
        if($this->products->get_by_id($id))
        {
            $this->data['product']      = $this->products->find($id);
            $this->data['categories']   = $this->categories->get();
            $this->data['title']        = "Editar ".$this->data['product']->nome;

            $this->template->load('panel/template_panel_new', 'panel/product_edit', $this->data);
        }
        else
        {
            $this->session->set_flashdata('error_message', '<strong>Atenção!</strong> Este produto não existe no banco de dados');
            redirect(panel_anchor('products'));
            exit();
        }
    }
    public function add()
    {
        $this->data['title']        =   'Adicionar novo Produto';
        $this->data['categories']   = $this->categories->get();
        $this->template->load('panel/template_panel_new', 'panel/product_add', $this->data);

    }
    public function update()
    {

        $config['upload_path'] = './public/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= 0;
        $config['max_width']  = 0;
        $config['max_height']  = 0;
        $this->load->library('upload', $config);

        if(!empty($_POST))
        {


            $update_product = $this->input->post();
            $update_product['valor'] = formataValorReal($this->input->post('valor'));

            if($this->upload->do_upload())
            {   
                $data = array('upload_data' => $this->upload->data());
                $update_product['imagem']   = $data['upload_data']['file_name'];

            }
            // Verifica se é um update ou insert
                if ($this->input->post('id_produto')) 
                {
                 

                     // verifica o status do update
                    if($this->products->update($this->input->post('id_produto'),  $update_product))
                    {
                        $this->session->set_flashdata('success_message', '<strong>Tudo certo!</strong> Produto atualizado com sucesso!');
                        redirect(panel_anchor("products/edit/{$update_product['id_produto']}"));
                    }
                    else
                    {
                        $this->session->set_flashdata('error_message', '<strong>Atenção!</strong> Houve um erro para atualizar o produto');
                        redirect(panel_anchor('products'));
                    }
                } 
                else 
                {
                    if($this->products->add($update_product))
                    {
                        $this->session->set_flashdata('success_message', '<strong>Tudo certo!</strong> Produto adicionado com sucesso!');
                        
                        redirect(panel_anchor("products"));
                    }
                    else
                    {
                        $this->session->set_flashdata('error_message', '<strong>Atenção!</strong> Houve um erro para adicionar o produto');
                        redirect(panel_anchor('products'));
                    }
                }
               

        }


    }


}