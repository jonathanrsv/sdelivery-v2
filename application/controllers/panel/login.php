<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller_Panel {
    private $redirect_area = 'panel/dashboard';

	public function __construct(){
        parent::__construct();
        $this->data['title'] = 'Acesso';
        
    }

    public function index(){
        if($this->login->isLogged()) 
        {
            redirect($this->redirect_area);
            exit();
        }
        $this->template->load('panel/template_login', 'panel/template_blank', $this->data);
        if ($this->input->post()) {
            $this->logar('panel');
        }
    }
	
	private function logar(){
        // VALIDATION set_rules
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        // MODELO MEMBERSHIP
        
        $query = $this->login->validate();

        if ($this->form_validation->run() == FALSE){  
            $this->session->set_flashdata('info_message', 'Por favor, Preencha todos os campos'); 
            redirect('panel/login');
        } else {

            if ($query){ // VERIFICA LOGIN E SENHA
                $data = array(
                    //'id' => $query->id,
                    //'username' => $this->input->post('username'),
                    'logged_panel' => true
                );
                $this->session->set_userdata($data);
                if($query->panel == 1) {
                    // se for administrador, leva pro painel
                    redirect($this->redirect_area);
                } else {
                    redirect(site_anchor());
                }

            } else {
                $this->session->set_flashdata('error_message', 'Login ou senha incorretos');
                redirect('panel/login');
                
            }
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('site');
    }

}

