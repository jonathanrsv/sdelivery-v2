<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends MY_Controller_Panel
{
    public function __construct()
    {
        parent::__construct();
        $this->data['page_ref'] = 'payment';
        $this->load->model('payment_model', 'payment');
        $this->verifyAuth();
    }

    public function index()
    {
        $this->data['title']        =   'Categorias';
        $this->data['formaspag']   =   $this->payment->get();
        $this->template->load('panel/template_panel_new', 'panel/payment_view', $this->data);
    }

    public function remove($id)
    {
        if(!empty($id))
        {
            if($this->payment->remove($id))
            {
                $this->session->set_flashdata('success_message', 'Forma de pagamento removida com sucesso');
                redirect(panel_anchor('payment'));
            }
            else
            {
                $this->session->set_flashdata('error_message', 'Ocorreu um erro ao remover a forma de pagamento!');
                redirect(panel_anchor('payment'));
            }
        }
        else
        {
            $this->session->set_flashdata('info_message', 'Você precisa selecionar uma forma de pagametno');
            redirect(panel_anchor('payment'));
        }

    }

    public function add()
    {
        $this->payment->add();
        $data['status'] = 'tra_ok';
        echo json_encode($data);
    }
}