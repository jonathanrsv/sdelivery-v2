<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller_Panel 
{
	public function __construct()
    {
        parent::__construct();
        $this->data['page_ref'] = 'dashboard';
        $this->load->model('order_itens_model', 'order_itens');
        $this->load->model('orders_model', 'orders');
        $this->verifyAuth();
    }
    public function index()
    {
        $this->data['title']        = 'DashBoard';
        $this->data['orders']       = $this->orders->get();
        $this->template->load('panel/template_panel_new', 'panel/orders_list', $this->data);
    }

    public function pedido($id = null)
    {
        if(!$this->orders->find($id))
        {
            $this->session->set_flashdata('error_message', '<strong>Atenção:</strong> Ocorreu um erro na sua solicitação, o pedido não existe.');
            redirect('panel/dashboard');
            exit();
        }

        $this->order_itens->initialise($id);
        $this->data['title']        = 'Visualização de Pedido';
        $this->data['order']        = $this->orders->find($id);
        $this->data['order_itens']  = $this->order_itens->find();
        $this->data['log_pedido']   = $this->db->query("SELECT tb_log_pedido.created, tb_status_pedido.nome AS status
                                                        FROM tb_log_pedido
                                                        INNER JOIN tb_status_pedido ON tb_log_pedido.id_status_pedido = tb_status_pedido.id_status_pedido
                                                        WHERE tb_log_pedido.id_pedido = {$id}
                                                        ORDER BY created DESC LIMIT 5")->result();

        $this->template->load('panel/template_panel_new', 'panel/order_view', $this->data);
    }

    public function alterar_status()
    {
        if($this->input->get('pedido'))
        {
            $data['id_status_pedido'] = $this->input->get('status');
            $query = $this->orders->update($data, $this->input->get('pedido'));

            if($query > 0)
            {
                $data_log = array(
                    'id_pedido'             => $this->input->get('pedido'),
                    'id_status_pedido'      => $this->input->get('status')
                );
                $this->db->insert('tb_log_pedido', $data_log);

                $this->session->set_flashdata('success_message', " O status do seu pedido foi alterado com sucesso!");
                redirect("panel/dashboard/pedido/{$this->input->get('pedido')}");
            }
            else
            {
                $this->session->set_flashdata('error_message', '<strong>Atenção!</strong> Houve um erro para alterar este pedido');
                redirect("panel/dashboard/pedido/{$this->input->get('pedido')}");
            }
        }
        else
        {
            $this->session->set_flashdata('error_message', '<strong>Atenção!</strong> Esta operação não pode ser realizada');
            redirect("panel/dashboard");
        }

    }
}
