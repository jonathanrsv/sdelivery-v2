<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends MY_Controller_Panel
{
    public function __construct()
    {
        parent::__construct();
        $this->data['page_ref'] = 'customers';
        $this->load->model('customers_model', 'customers');
        $this->verifyAuth();
    }

    public function index()
    {
        $filter = array();
        if(isset($_GET['nome']))
        {
            $filter['nome']       = $this->input->get('nome');
        }

        $this->data['clientes'] =   $this->customers->get($filter);
        $this->data['title'] = 'Clientes';
        $this->template->load('panel/template_panel_new', 'panel/customers_view', $this->data);
    }

    public function edit($id)
    {
      if(!empty($id))
      {
        if(!$this->data['customer'] =  $this->customers->find($id))
        {
          $this->session->set_flashdata('error_message', 'Usuario selecionado não existe');
            redirect(panel_anchor('customers'));
        }
      
        $this->template->load('panel/template_panel_new', '/panel/customer_edit_view', $this->data);
      }
    }



    public function remove($id)
    {
        if(!empty($id))
        {
            if($this->customers->remove($id))
            {
                $this->session->set_flashdata('success_message', 'Usuario removido com sucesso');
                redirect(panel_anchor('customers'));
            }
            else
            {
                $this->session->set_flashdata('error_message', 'Ocorreu um erro ao remover o usuario!');
                redirect(panel_anchor('customers'));
            }
        }
        else
        {
            $this->session->set_flashdata('error_message', 'Você precisa selecionar um usuario para deleta-lo');
            redirect(panel_anchor('customers'));
        }

    }
}
