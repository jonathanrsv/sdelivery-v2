<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MY_Controller_Panel
{
    public $page_ref    = 'pages';
    public $page        = 'Páginas';

	public function __construct()
    {
        parent::__construct();
        
        $this->load->model('car_detail_model', 'car_detail');
        $this->data['title']    = 'Páginas';
        $this->data['page_ref'] = $this->page_ref;
        $this->data['page']     = $this->page;
        $this->verifyAuth();
        
    }

    public function index()
    {
    	$this->data['title'] = 'Todas paginas';        
    	$this->template->load('panel/template_panel', 'panel/view_pages', $this->data);
    }

    public function edit($id)
    {
    	$this->data['title'] = 'Editar Página';
    	$this->data['page']  = $this->pages->get_by_id($id);
    	$this->template->load('panel/template_panel_new', 'panel/edit_page', $this->data);
    }
    public function opa()
    {
        $this->template->load('panel/template_panel_new', 'panel/view_pages', $this->data);
        // dump($this->session->userdata);
    }
} 