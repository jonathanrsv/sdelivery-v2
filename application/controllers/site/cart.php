<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends MY_Site
{

    public function __construct()
    {
        parent::__construct();          
               
        
    }
    public function index()
    {
        echo "cart pode pah";
        
    }
    public function add($product_id = null)
    {
        $product = $this->products->find($product_id);
        //dump($product);
        if(!empty($product))
        {
            $data = array(
                'id'      =>  $product->id_produto,
                'qty'     =>  1,
                'qty_disp'=>  $product->qtd_estoque,
                'price'   =>  $product->valor,
                'name'    =>  $product->nome,
                'imagem'  => $product->imagem,
            );
            $insert_cart = $this->cart->insert($data);

            if($insert_cart)
            {
               $this->session->set_flashdata('success_message', "Produto adicionado com sucesso!");
               redirect( site_anchor('carrinho/view'));

            }

        }
        else
        {
            $this->session->set_flashdata("error_message", "<strong>Ops!</strong> Este produto não existe");
            redirect(site_anchor('carrinho/view'));
        }


    }
    public function view()
    {
        
        $this->data['digitalData'] = $this->digitaldata->mountCart();

        $this->data['title']    = "Carrinho de compras";
        $this->data['heading']  = "Carrinho de compras";
        $this->data['cart_itens'] = $this->cart->contents();
        $this->template->load('site/template_site', 'site/cart_view', $this->data);
    }

    public function remove($rowid = null)
    {
        $data_cart = array(
            'rowid' => $rowid,
            'qty'   => 0
        );

        if($this->cart->update($data_cart)){
            $this->session->set_flashdata("success_message", "Você removeu o produto do carrinho");
            redirect(site_anchor('carrinho/view'));
        } else {
            $this->session->set_flashdata("error_message", "Ocorreu um erro ao remover o item selecionado do carrinho");
            redirect(site_anchor('carrinho/view'));
        }
    }
    public function update()
    {
        foreach ($this->input->post('qty') as $key => $value){
           $data_cart =  array(
               'rowid' => $key,
               'qty'   => $value
           );
           $this->cart->update($data_cart);
        }

        $this->session->set_flashdata("success_message", "Carrinho de compras atualizado!");
        redirect(site_anchor('carrinho/view'));

    }

    public function address(){       

        if (!$this->login->isLogged()) {
            // $this->session->set_flashdata("error_message", "Por favor, efetue login ou cadastre-se");
            // redirect(site_anchor('site'));
            // exit();

            $this->session->set_flashdata("error_message", "Por favor, efetue login ou cadastre-se");
            $this->template->load('site/template_site', 'site/register_view', $this->data);
        }

        if($this->input->post("id_endereco"))
        {
            $checkout = array("checkout" => array('id_endereco' =>  $this->input->post("id_endereco")));
            $this->session->set_userdata($checkout);
        }

        $this->data['title']    = "Escolha um endereço de entrega";
        $this->data['heading']  = "Endereço de entrega";
        $this->data['client_address'] = $this->customers->getAddressById($this->session->userdata('id_cliente'));

        $this->data['digitalData'] = $this->digitaldata->mountCart('address');

        $this->template->load('site/template_site', 'site/cart_address', $this->data);
    }
    
    public function paymentMethod()
    {
        if(!$this->session->userdata('id_endereco'))
        {
            $this->session->set_flashdata("error_message", "Por favor, selecione um endereço de entrega");
            redirect(site_anchor('carrinho/endereco'));
            exit();
        }

        $this->data['title']    = "Escolha um método de pagamento";
        $this->data['heading']  = "Métodos de pagamento";
        $this->data['pagamentos'] = $this->payment->get();
        $this->data['digitalData'] = $this->digitaldata->mountCart('paymentMethod');

        $this->template->load('site/template_site', 'site/cart_payment', $this->data);      
    }
    public function confirmation()
    {
        $this->data['title']    = "Confirme seu pedido";
        $this->data['heading']  = "Confirme seu pedido";
        $this->data['cart_itens'] = $this->cart->contents();
        $this->data['payment'] =  $this->payment->getId($this->session->userdata('id_formapag'))->nome;
        $this->data['totalCart'] = $this->cart->total();
        $this->data['frete'] = $this->options->get('frete')->option_valor;
        $this->data['total'] = $this->cart->total() + $this->options->get('frete')->option_valor;

        $this->data['digitalData'] = $this->digitaldata->mountCart('confirmation');
        
        $this->template->load('site/template_site', 'site/cart_confirmation', $this->data);   
    }

    public function purchase()
    {

        $this->data['title']    = "Pedido Finalizado";
        
        if(!$this->cart->contents())
        {
            $this->session->set_flashdata("error_message", "Algo deu errado e seu carrinho está vazio :(");
            redirect(site_anchor());
            exit();    
        }

        if(!$this->session->userdata('id_formapag') OR !$this->session->userdata('id_endereco'))
        {
            $this->session->set_flashdata("error_message", "Por favor, você deve selecionar um endereço de entega e um metodo de pagamento");
            redirect(site_anchor('carrinho'));
            exit();
        }
        
        $orderId = $this->order->returnLastId() + 1;

        $order = array(
            'id_cliente' => $this->session->userdata('id_cliente'),
            'id_formapag' => $this->session->userdata('id_formapag'),
            'id_endereco' => $this->session->userdata('id_endereco'),
            'id_telefone' => 1,
            'id_status_pedido' => 4,
            'frete' => $this->options->get('frete')->option_valor,
            'obs' => 'fazer', 
            'id_pedido' => $orderId
        );



        $itens = $this->cart->contents(); 

        foreach ($itens as $key => $item) {

                $order_itens = array(
                    'id_produto' => $item['id'],
                    'qtd' => $item['qty'],
                    'valor_unitario' => $item['price'],
                    'desconto_unitario' => 0,
                    'id_pedido' => $orderId
                );
                
                $this->order_itens->insert($order_itens);
           
        }

        $this->order->insert($order);

        $this->data['digitalData'] = $this->digitaldata->mountCart('purchase');

        $this->cart->destroy();

        $this->session->unset_userdata('id_formapag');
        $this->session->unset_userdata('id_endereco');
        
        $this->template->load('site/template_site', 'site/cart_purchase', $this->data);

    }

    public function destroy()
    {
        $this->cart->destroy();
    }
}