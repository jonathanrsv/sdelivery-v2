<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends MY_Site
{

	function __construct()
    {
        parent::__construct();
        $this->load->model('products_model', 'products');
        $this->load->model('options_model', 'options');
        $this->load->library('cart');
        $this->load->model('categories_model', 'categories');
        $this->load->helper('file');
        $this->data['categories']   = $this->categories->get();

    }

    public function feed()
    {

       echo 'test';

    }

	public function index()
	{
        $this->data['title']        = "Página inicial";
        $this->data['products']     = $this->products->get();

        $params = array(
                'name' => 'Home',
                'template' => 'HOME',
                'catalog' => 'HOME',
                );

        $this->data['digitalData'] = $this->digitaldata->mountPage($params);

        $this->template->load('site/template_site', 'site/inicial_view', $this->data);

	}
    public function category($slug = null)
    {
        $this->data['title']    =   "Categoria: {$this->categories->find_by_slug($slug)->nome}";
        $this->data['heading']  =   $this->categories->find_by_slug($slug)->nome;
        $this->data['products'] = $this->products->get_by_category_slug($slug);

        $params = array(
                'name' => $this->categories->find_by_slug($slug)->nome,
                'template' => 'CATALOG',
                'nameDepartment' => $this->categories->find_by_slug($slug)->nome,
                );

        $this->data['digitalData'] = $this->digitaldata->mountPage($params);

        $this->template->load('site/template_site', 'site/category_view', $this->data);

    }
    public function product($slug = null)
    {
        $this->data['products'] = $this->products->get(array('limit' => 3));
        $this->data['title']    = $this->products->get_by_slug($slug)->nome;
        $this->data['product'] = $this->products->get_by_slug($slug);


         $params = array(
                'name' => $this->products->get_by_slug($slug)->nome,
                'template' => 'PRODUTO',
                'nameDepartment' =>  $this->categories->find($this->products->get_by_slug($slug)->id_categoria)->nome,
                'product' => array(
                        array(
                            'basePrice' => $this->products->get_by_slug($slug)->valor,
                        'nameDepartment' => $this->categories->find($this->products->get_by_slug($slug)->id_categoria)->nome,
                        'discountPercentage' => 0,
                        'category' => $this->categories->find($this->products->get_by_slug($slug)->id_categoria)->nome,
                        'fullName' => $this->products->get_by_slug($slug)->nome,
                        'idSku' => $this->products->get_by_slug($slug)->id_produto,
                        'name' => $this->products->get_by_slug($slug)->nome,
                        'salePrice' => $this->products->get_by_slug($slug)->valor,
                        'availability' => true
                        )

                    )
                );

        $this->data['digitalData'] = $this->digitaldata->mountPage($params);

        $this->template->load('site/template_site', 'site/product_view', $this->data);
    }

    public function search()
    {
        $this->data['title'] = 'Busca';

        if($this->input->get('s'))
        {
            $stringQuery = $this->input->get('s');
            $this->data['heading']  =   'Buscando por: ' . $stringQuery;
            $this->data['products'] = $this->products->get(array('nome' => $stringQuery));

            $search = array('keyword' => $stringQuery, 'results' => count($this->data['products']));
            $params = array(
                'name' => 'search',
                'template' => 'SEARCH',
                'search' => array(
                    'keyword' => $stringQuery,
                    'results' => count($this->data['products']),
                    'search_type' => 'search_results'
                    )
                );


            $this->data['digitalData'] = $this->digitaldata->mountPage($params);

            $this->template->load('site/template_site', 'site/search_view', $this->data);
        }
        else
        {
            $this->session->set_flashdata('error_message', "Você precisa buscar por algo :(");
            redirect( site_anchor(''));
        }

    }

    public function register($save  = null)
    {
        $this->data['title'] = 'Registro';
        if ($save)
        {
           if ($this->input->post('email') === '' || $this->input->post('senha') === '')
            {
               $this->session->set_flashdata('error_message', "Preencha todos os campos para completa seu cadastro");
               redirect( site_anchor('cadastro'));
               exit();
            }
            else
            {
                $data['nome'] = $this->input->post('nome');
                $data['senha'] =  md5($this->input->post('password'));
                $data['email'] = $this->input->post('username');
                $data['cpf'] = $this->input->post('cpf');
                $data['datanasc'] = $this->input->post('datanasc');
                $data['status'] = true;

                if ($this->customers->insert($data))
                {
                    $this->session->set_flashdata('success_message', "Oh que legal! seu cadastro foi realizado :)");

                    $logar = $this->login->validate();
                    $this->session->set_userdata(array( 'logged_panel' => true));

                    redirect( site_anchor('cadastro/endereco'));
                }
            }

        }

        $params = array(
                'name' => 'Cadastro',
                'template' => 'CHECKOUT',
                );

        $this->data['digitalData'] = $this->digitaldata->mountPage($params);

        $this->template->load('site/template_site', 'site/register_view', $this->data);
    }

    public function address()
    {
        $this->protectArea();
        $this->data['title'] = 'Cadastre um ou mais endereços de entrega';
				$adress = $this->data['client_address'] = $this->customers->getAddressById($this->session->userdata('id_cliente'));
        if($this->input->post())
        {
            $data = $this->input->post();
            $data['id_cliente'] = $this->session->userdata('id_cliente');

            if($this->customers->insertAddress($data))
            {
                $this->session->set_flashdata('success_message', "Seu novo endereço foi cadastrado!");
                redirect( site_anchor('cadastro/endereco'));
            }

        }

         $params = array(
                'name' => 'Novo Endereço',
                'template' => 'CHECKOUT',
                );

        $this->data['digitalData'] = $this->digitaldata->mountPage($params);

        $this->template->load('site/template_site', 'site/register_address_view', $this->data);

    }

    public function contato()
    {
        echo "contatos chamadoa";
    }

    public function protectArea()
    {
         if (!$this->login->isLogged()) {
            $this->session->set_flashdata("error_message", "Por favor, efetue login ou cadastre-se para acessar esta area.");
            redirect(site_anchor('site'));
            exit();
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
