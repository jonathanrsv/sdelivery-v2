<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller
{ 
	public function distanceAdress(){
		$origem = $this->input->get('origem');
		$cepdestino = $this->input->get('destino');
		$filename = 'http://maps.googleapis.com/maps/api/distancematrix/json?origins='.$origem.'&destinations='.$cepdestino.'&sensor=false&language=PT';

        $json = file_get_contents($filename);  
        echo $json;
	}

	public function setSession()
	{
		$this->session->set_userdata($this->input->post());

	}



}