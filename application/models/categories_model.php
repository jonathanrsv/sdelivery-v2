<?php
class categories_model extends CI_Model
{
    private $table  =   'tb_categorias';
    private $key    =   'id_categoria';

    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        $this->db->where('status', 1);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function find($id)
    {
        $this->db->where($this->key, $id);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function find_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function remove($id)
    {
        $data['status'] = 0;
        $this->db->where('id_categoria', $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function add()
    {
        $data['nome']   = $this->input->post('result');
        $data['slug']   = slugify($this->input->post('result'));
        $data['status'] = 1;
        $this->db->insert($this->table, $data);
        return $this->db->affected_rows();
    }
}