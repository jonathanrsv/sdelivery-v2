<?php
class products_model extends CI_Model
{
    private $table  = 'tb_produtos';
    private $key    = 'id_produto';

    public function __construct()
    {
        parent::__construct();
    }

    public function get($filter = array())
    {
        
        if(isset($filter['nome']))
        {
            $this->db->like('nome', $filter['nome']);
        }
        
        $this->db->where('status', 1);

        if(isset($filter['limit']))
        {           
            $query = $this->db->get($this->table, $filter['limit']);
        } 
        else 
        {
            $query = $this->db->get($this->table);
        }

        
        return $query->result();
    }

    public function add($data)
    {
        /*$data['nome']   =  $this->input->post('nome');
        $data['slug']   = slugify($this->input->post('nome'));
        $data['descricao'] = $this->input->post('descricao');      
        $data['qtd_estoque']  = $this->input->post('qtd_estoque');
        $data['id_categoria']  = $this->input->post('id_categoria');
        $data['valor']  = $this->input->post('valor');
        $data['imagem']  = $this->input->post('imagem');
        $data['status'] = 1; */
        $data['slug_produto']   = slugify($this->input->post('name'));
        $data['status'] = 1;
        $this->db->insert($this->table, $data);
        return $this->db->affected_rows();
    }

    public function find($id)
    {
        $this->db->where($this->key, $id);
        $query = $this->db->get($this->table);
        return $query->row();
    }
    public function get_by_slug($slug)
    {
        $this->db->where('slug_produto', $slug);
        $query = $this->db->get($this->table);
        return $query->row(); 
    }

    public function get_by_id($id)
    {
        $this->db->where($this->key, $id);
        $query = $this->db->get($this->table);
        return $query->num_rows();
    }
    public function get_by_category_slug($slug)
    {
        $this->db->select('tb_categorias.slug as slug_categoria, tb_produtos.*');
        $this->db->join('tb_categorias', 'tb_produtos.id_categoria = tb_categorias.id_categoria', 'inner');
        $this->db->where('slug', $slug);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function count_by_category($id)
    {
        $this->db->where('id_categoria', $id);
        $this->db->where('status', 1);
        $query =  $this->db->get($this->table);
        return $query->num_rows();
    }
    public function update($id, $data)
    {
        $data['slug_produto']   = slugify($this->input->post('nome'));
        $this->db->where($this->key, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }
}
