<?php
class orders_model extends CI_Model
{
    private $table  = 'tb_pedidos';
    private $key    = 'id_pedido';
    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        $query = $this->db->query("SELECT tb_pedidos.*, tb_clientes.*, tb_status_pedido.nome AS status_pedido FROM tb_pedidos
        INNER JOIN tb_clientes  ON tb_pedidos.id_cliente = tb_clientes.id_cliente
        INNER JOIN tb_status_pedido ON tb_pedidos.id_status_pedido = tb_status_pedido.id_status_pedido
        ORDER BY data_pedido DESC");

        return $query->result();
    }

    public function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->affected_rows();
    }

    public function returnLastId()
    {
        $query = $this->db->query("SELECT max({$this->key}) AS lastid FROM {$this->table}");
        return $query->row('lastid');
    }

    public function find($id)
    {
        $query = $this->db->query("SELECT tb_pedidos.*, tb_clientes.nome AS nome_cliente, tb_clientes.email AS email, tb_formaspag.nome AS form_pagamento, tb_enderecos.endereco AS rua,
        tb_enderecos.numero AS n_endereco, tb_enderecos.complemento, tb_enderecos.referencia, tb_telefone.telefone, tb_status_pedido.nome AS status_pedido
        FROM tb_pedidos
        INNER JOIN tb_clientes ON tb_pedidos.id_cliente = tb_clientes.id_cliente
        INNER JOIN tb_formaspag ON tb_pedidos.id_formapag = tb_formaspag.id_formapag
        INNER JOIN tb_enderecos ON tb_pedidos.id_endereco = tb_enderecos.id_endereco
        INNER JOIN tb_telefone ON tb_pedidos.id_telefone = tb_telefone.id_telefone
        INNER JOIN tb_status_pedido ON tb_pedidos.id_status_pedido = tb_status_pedido.id_status_pedido
        WHERE id_pedido = $id
        ");
        return $query->row();
    }

    public function update($data, $id)
    {
        $this->db->where($this->key, $id);
        $this->db->update($this->table, $data);
        return  $this->db->affected_rows();
    }

}
