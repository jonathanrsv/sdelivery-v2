<?php
class payment_model extends CI_Model
{
    private $table  = 'tb_formaspag';
    private $key    = 'id_formapag';

    public function __construct()
    {
        parent::__construct();
    }

    public function get()
    {
        $this->db->where('status', 1);
        $query = $this->db->get($this->table);
        return $query->result();
    }

     public function getId($id)
    {
        $this->db->where('id_formapag', $id);
        $query = $this->db->get($this->table);
        return $query->row();

    }

    public function remove($id)
    {
        $data['status'] = 0;
        $this->db->where($this->key, $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

    public function add()
    {
        $data['nome']   = $this->input->post('result');
        $data['status'] = 1;
        $this->db->insert($this->table, $data);
        return $this->db->affected_rows();
    }
}