<?php
class order_itens_model extends CI_Model
{
    private $table = 'tb_pedidos_itens';
    private $id_order;

    public function __construct()
    {
        parent::__construct();

    }
    public function initialise($id_order)
    {
        $this->id_order = $id_order;
    }
    public function find()
    {
        $query = $this->db->query("SELECT * FROM tb_pedidos_itens INNER JOIN tb_produtos ON tb_pedidos_itens.id_produto = tb_produtos.id_produto WHERE tb_pedidos_itens.id_pedido = $this->id_order");
        return $query->result();
    }

    public function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->affected_rows();
    }

    public function  sum_order()
    {
        $query = $this->db->query("SELECT sum(valor_unitario * qtd) - sum(desconto_unitario) as total  FROM tb_pedidos_itens WHERE id_pedido =  $this->id_order");
        return $query->row();
    }

}