<?php
class customers_model extends CI_Model
{
	private $table = 'tb_clientes';

	public function __construct()
    {
        parent::__construct();
    }

    public function get($filter = array())
    {
        if(isset($filter['nome']))
        {
            $this->db->like('nome', $filter['nome']);
        }
        $this->db->where('status', 1);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function find($id)
    {
        $this->db->where('id_cliente', $id);
        $query = $this->db->get($this->table);
        return $query->row();
    }
    public function insert($data)
    {
       $query =  $this->db->insert($this->table, $data);
       return $query;
    }
    public function insertAddress($data)
    {
        $query =  $this->db->insert('tb_enderecos', $data);
        return $query;
    }
    public function getAddressById($id)
    {
        $this->db->where('id_cliente', $id);
        $query = $this->db->get('tb_enderecos');
        return $query->result();
    }

    public function remove($id)
    {
        $data['status'] = 0;
        $this->db->where('id_cliente', $id);
        $this->db->update($this->table, $data);
        return $this->db->affected_rows();
    }

}

