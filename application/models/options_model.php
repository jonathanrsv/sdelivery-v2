<?php
class options_model extends CI_Model
{
    private $table = 'tb_opcoes';

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        $this->db->select('option_nome, option_valor');
        $query = $this->db->get($this->table);
        return $query->row();
    }
    public function get($option_name)
    {
        $this->db->where('option_nome', $option_name);
        $query = $this->db->get($this->table);
        return $query->row();

    }

    public function set($option_key, $value)
    {
        $data['option_valor'] = $value;
        $this->db->where('option_nome', $option_key);
        $this->db->update($this->table, $data);

    }

}
