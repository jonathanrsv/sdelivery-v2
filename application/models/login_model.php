<?php
class Login_model extends CI_Model {

    private $table = 'tb_clientes';
    # valida usuario
    public function validate() 
    {
        $this->db->where('email', $this->input->post('username'));
        $this->db->where('senha', md5($this->input->post('password')));
        $this->db->where('status', 1); // Verifica o status do usuário

        $query = $this->db->get($this->table);

        if ($query->num_rows == 1) 
        {
            $this->session->set_userdata($query->row());
            return $query->row(); // retorna verdadeiro
        } 
    }

    # verifica se o usuario está logado
    public function isLogged()
    {
        $logged = $this->session->userdata('logged_panel');


        if (!isset($logged) || $logged != true) 
        {
            return false;
        } 
        else 
        {
            if($this->data['area'] == 'panel' && $this->session->userdata['panel'] == 0)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
    }
}