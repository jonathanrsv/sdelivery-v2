<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller_Panel extends CI_Controller
{
	public $data = array();
	public $alert;

	public function __construct() 
	{
	    parent::__construct();
	    
	    /* Declaração com nomenclatura de models que serão usados em N partes do sistema */
	    $this->load->model('login_model', 'login');
        $this->load->model('options_model', 'option');

		
		/* Declaração de variaveis globais que serão usadas em N partes do sistema */
	    $this->data = array(
                'area'              => 'panel',
	    		'project_name'		=> $this->config->item('project_name'),
                'error_message' 	=> $this->session->flashdata('error_message'),
                'info_message' 		=> $this->session->flashdata('info_message'),
                'success_message' 	=> $this->session->flashdata('success_message'),
        );

	}
	public function verifyAuth()
	{
		if (!$this->login->isLogged('panel'))
		{
            $this->session->set_flashdata('error_message', '<strong>Ops!</strong> Algo deu errado seu usuario e senha estão incorretos ou você não tem permissão.');
			redirect('panel/login');
			exit();   
		} 
	}
	public function erro_404()
	{
		echo "pagina não encontrada!";
	} 
	public function teste()
	{
		print_r($this->data['pages']);

	}

}