<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Site extends CI_Controller
{
    public $data = array();
    public $alert;

    public function __construct()
    {
        parent::__construct();
        $this->load->library('cart');
        $this->load->model('login_model', 'login');
        $this->load->model('options_model', 'options');
        $this->load->model('payment_model', 'payment');
        $this->load->model('orders_model', 'order');
        $this->load->model('order_itens_model', 'order_itens');
        $this->load->model('categories_model', 'categories');
        $this->load->model('products_model', 'products');  
        $this->load->model('customers_model', 'customers');
        $this->load->library('Digitaldata');

        $this->data = array(
            'area'              => 'site',
            'project_name'		=> $this->config->item('project_name'),
            'error_message' 	=> $this->session->flashdata('error_message'),
            'info_message' 		=> $this->session->flashdata('info_message'),
            'success_message' 	=> $this->session->flashdata('success_message'),
            'categories'        => $this->categories->get(),
            'digitalData'       => $this->digitaldata->mountDefault()
        );

    }
    public function verifyAuth()
    {
        if (!$this->login->isLogged('panel'))
        {
            $this->session->set_flashdata('error_message', '<strong>Ops!</strong> Algo deu errado, seu usuario e senha estão incorretos ou você não tem permissão.');
            redirect('login');
            exit();
        }
    }
    public function erro_404()
    {
        echo "pagina não encontrada!";
    }

    public function teste()
    {
       echo "controller site chamado";

    }

}