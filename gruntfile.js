"use strict";

module.exports = function( grunt ) {

  
  require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
  grunt.initConfig({

    'ftp-deploy': {
      build: {
        auth: {
          host: 'jonathansilva.me',
          port: 21,
          authKey: 'key1'
        },
        src: 'C:/git/sdelivery-v2',
        dest: '/public_html/sDelivery/',
        exclusions: ['C:/git/sdelivery-v2/.git']
      }
    }

    // configuracoes das tarefas

  });

  // carregando plugins
  //grunt.loadNpmTasks( 'plugin-name' );

  // registrando tarefas
  grunt.registerTask( 'deploy', [ 'ftp-deploy' ] );

};