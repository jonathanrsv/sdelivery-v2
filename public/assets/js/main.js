$('#save').on('click', function (e) {
    e.preventDefault();
    $('#submitThis').submit();
});

var maskFormat = function () {
    $('[name=hora_final]').mask('99:99');
    $('[name=hora_inicial]').mask('99:99');
    $('[name=cep_cliente]').mask('99999-999');
    $('[name=frete]').mask('9,99');

    $('[name=telefone_contato]').mask('(99) 9999-9999');
}();

var moments = function (){
    moment.lang('pt-br');
    $('.date_order').each(function(){
        var a = $(this).text();
        console.log(a);
        $(this).html(moment(a).startOf('day').fromNow());
    });
}();

$('.simple-insert-categories').on('click', function (e) {
    e.preventDefault();
    //blur.on();
    page = $(this).data('page');
    id_group = $(this).data('id-group');

    bootbox.prompt("Qual será o nome da categoria?", function (result) {
        if (result === null) {
            notify('Você cancelou.');
        } else {
            $.ajax({
                type: "POST",
                url: page + '/add/',
                data: {
                    result: result,
                    page: page
                },
                dataType: 'json'
            }).done(function (data) {
                if (data.status == 'tra_ok') {
                    notify('Categoria cadastrada :)');
                    window.location.href = window.location.href;
                } else {
                    notify(data.message, 'error');
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {

                notify('Ops... Ocorreu um erro inesperado, atualize a pagina e tente novamente por favor.')
            })
        }
    });
});

$('.simple-insert-payment').on('click', function (e) {
    e.preventDefault();
    //blur.on();
    page = $(this).data('page');
    id_group = $(this).data('id-group');

    bootbox.prompt("Qual será o nome da forma de pagamento?", function (result) {
        if (result === null) {
            notify('Você cancelou.');
        } else {
            $.ajax({
                type: "POST",
                url: page + '/add/',
                data: {
                    result: result,
                    page: page
                },
                dataType: 'json'
            }).done(function (data) {
                if (data.status == 'tra_ok') {
                    window.location.href = window.location.href;
                } else {
                    notify(data.message, 'error');
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {

                notify('Ops... Ocorreu um erro inesperado, atualize a pagina e tente novamente por favor.')
            })
        }
    });
});




var notify = function (msg, type) {
    switch (type) {
        case 'success':
            alertify.success(msg);
            break;
        case 'error':
            alertify.error(msg);
            break;
        default:
            alertify.log(msg);
            break;
    }
};

$('#save').on('click', function (e) {
    e.preventDefault();

    $('#submitThis').submit();
    });






