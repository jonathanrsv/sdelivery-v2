function searchProduct(search_string){
	if (!search_string){
		return false;
	}

	window.location.href = digitalData.site.domain + 'busca/?s=' + search_string;
}

jQuery('input[name=id_endereco]').on("click", function(){

	 var url = 'http://jonathansilva.me/sDelivery/ajax/distanceAdress/?origem='
              + digitalData.site.cep_origem
              + '&destino='
              + $(this).data('cep') 
              + '&sensor=false&language=PT';

	$.ajax({
			url: url,
			dataType: "text",
			success: function (data) {
				var json = $.parseJSON(data);
				console.log(json);
			}
		});
   

});

jQuery('#stringSearch').keyup(function(e){
	if(e.keyCode == 13){
		var stringSearch = $('#stringSearch').val();
		searchProduct(stringSearch);
	}
});

jQuery('#button-search').on("click", function() {
	var stringSearch = $('#stringSearch').val();
	searchProduct(stringSearch);
});


jQuery("#cep").on("change", function(){
	var cep_code = $(this).val();
	if( cep_code.length <= 0 ) return;

	 $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
         function(result){
            if( result.status!=1 ){
               alert(result.message || "Houve um erro desconhecido");
               return;
            }
            $("input#cep").val( result.code );
            $("input#estado").val( result.state );
            $("input#cidade").val( result.city );
            $("input#bairro").val( result.district );
            $("input#endereco").val( result.address );
            $("input#estado").val( result.state );

            $('#ocultaForm').show();

         });

});

jQuery( "input[name='id_endereco']" ).on("change", function(){

	$.post( digitalData.site.domain + 'ajax/setSession', { id_endereco: this.value } );

});

jQuery( "input[name='id_formapag']" ).on("change", function(){

	$.post( digitalData.site.domain + 'ajax/setSession', { id_formapag: this.value } );

});