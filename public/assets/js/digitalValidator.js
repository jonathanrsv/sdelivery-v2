var digitalValidator  = 
	{
		version: "20141222",
		requestURI: "http://dev.dp6.net/digitalValidator/?a=",
		dataSource: (typeof siteMetadata != 'undefined' ? siteMetadata : ''),
		error: null,

		exec: function()
		{
			digitalValidator.sendRequest();
		},

		sendRequest: function ()
		{			

			var request = new XMLHttpRequest();
			request.open("POST", digitalValidator.requestURI, true);
			request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			request.setRequestHeader("Connection", "close");
			

			request.onreadystatechange = function() { 
			    if(request.readyState == 4 && request.status == 200) {
			        console.log(request.responseText);
			    }
			}
			request.setRequestHeader("Content-length", digitalValidator.length);
			request.send('errors=' + digitalValidator.errors + '&a=' + btoa(JSON.stringify(digitalValidator.dataSource))); 
			console.info('Validação:Check');
		},
		randomNumber: function(max)
		{
			return Math.floor((Math.random() * max) + 1);
		},
		getCookie: function(sKey){
			return unescape(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*"+
		      escape(sKey).replace(/[\-\.\+\*]/g,"\\$&")+"\\s*\\=\\s*([^;]*).*$)|^.*$"),"$1")) || null ;
		},

		setCookie: function(name, value, expiredays){
			var exdate = new Date();
			exdate.setDate(exdate.getDate()+expiredays);
			document.cookie=name+ "=" +escape(value)+
			((expiredays===null) ? "" : ";expires="+exdate.toUTCString())+"; path=/";
		},

	} 